{**
 * 2007-2019 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2019 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
<div class="container cont-1300">
	<div class="o-footer-top">
		<div class="o-footer-top__categorys">
			<h3>Categorías:</h3>
			<ul>
				<li><a href="">Fijación y anclajes</a></li>
				<li><a href="">Herramienta eléctrica</a></li>
				<li><a href="">Corte taladro y cepillería</a></li>
				<li><a href="">Protección laboral y seguridad</a></li>
				<li><a href="">Pinceles y aceites</a></li>
				<li><a href="">Cintas y adhesivos</a></li>
				<li><a href="">Herramienta manual</a></li>
				<li><a href="">Escaleras</a></li>
				<li><a href="">Herramienta taller</a></li>
				<li><a href="">Soldadura</a></li>
				<li><a href="">Medición y escritura</a></li>
				<li><a href="">Cuerdas y cintas</a></li>
			</ul>
		</div>
		<div class="o-footer-top__info">
			<div class="info-top">
				<a href="mailto:info@topferreteria.es"><img src="{$urls.base_url}library/images/ico-mail-header.svg">info@topferreteria.es</a>
				<a href="tel:+ 34 694 904 053"><img src="{$urls.base_url}library/images/ico-tel-header.svg">+ 34 694 904 053</a>
			</div>
			<div class="info-pay">
				<span>Formas de pago:</span>
				<img src="{$urls.base_url}library/images/formas-pago.png" alt="Formas de pago">
			</div>
		</div>
	</div>
	<div class="sombra">
		<img src="{$urls.base_url}library/images/sombra.svg">
	</div>
	<div class="o-footer-bottom">
		<a href="" id="logo-footer"><img src="{$urls.base_url}library/images/logo-footer.svg"></a>
		<ul class="politicas">
			<li><a href="">Aviso legal</a></li>
			<li><span>|</span></li>
			<li><a href="">Política de privacidad</a></li>
			<li><span>|</span></li>
			<li><a href="">Política de cookies</a></li>
		</ul>
	</div>
</div>

{literal}
<script>
	// SLIDER HOME CABECERA
	$('.o-home-slider').slick({
		arrows: false,
		infinite: false,
		slidesToShow: 1,
		slidesToScroll: 1,
		dots: true,
	});

	// SLIDER PRODUCTOS HOME
	$('.o-featured-products__list').slick({
		arrows: true,
		infinite: false,
		slidesToShow: 4,
		slidesToScroll: 1,
		dots: false,
		slidesPerRow: 1,
		rows: 2,
		responsive: [
			{
				breakpoint: 1200,
				settings: {
					slidesToShow: 3,
				}
			},
			{
				breakpoint: 991,
				settings: {
					slidesToShow: 2,
				}
			},
			{
				breakpoint: 750,
				settings: {
					slidesToShow: 1,
					rows: 4,
					slidesPerRow: 1,
				}
			},
		]
	});

	// SLIDER MARCAS
	$('.o-marcas__slider').slick({
		arrows: true,
		infinite: false,
		slidesToShow: 5,
		slidesToScroll: 1,
		dots: false,
		responsive: [
			{
				breakpoint: 991,
				settings: {
					slidesToShow: 4,
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 2,
				}
			},
			{
				breakpoint: 550,
				settings: {
					slidesToShow: 1,
				}
			},
		]
	});
	$('.slider-productos-left').slick({
		dots: false,
		infinite: false,
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		asNavFor: '.slider-productos-right',
	});
	$('.slider-productos-right').slick({
		dots: false,
		infinite: false,
		slidesToShow: 3,
		slidesToScroll: 1,
		arrows: false,
		vertical: true,
		verticalSwiping: true,
		asNavFor: '.slider-productos-left',
		responsive: [
			{
				breakpoint: 991,
				settings: {
					vertical: false,
					verticalSwiping: false,
				}
			},
			{
				breakpoint: 768,
				settings: {
					vertical: false,
					verticalSwiping: false,
					slidesToShow: 2,
					slidesToScroll: 1,
				}
			},
		]
	});
	$(".js-range-slider").ionRangeSlider({
		type: "double",
		polyfill: false,
		min: 0,
		max: 350,
		from: 20,
		to: 100,
		grid: true
	});
</script>
{/literal}