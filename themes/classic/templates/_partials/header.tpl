{**
 * 2007-2019 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2019 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
<header class="c-header">
		<div class="header-top">
			<div class="container cont-1300">
				<span><img src="{$urls.base_url}library/images/ico-monedero-header.svg"> ¡Precios super, super, super ajustados!</span>

				<div class="">
					<a href="mailto:info@topferreteria.es"><img src="{$urls.base_url}library/images/ico-mail-header.svg">info@topferreteria.es</a>
					<a href="tel:694904053"><img src="{$urls.base_url}library/images/ico-tel-header.svg">+34 694 904 053</a>
				</div>
			</div>
		</div>

		<div id="menu">
			<section class="pre-menu-mobile">
        <div class="container cont-1300">
          <div class="mobile-menu-left">
              <a href="mailto:info@topferreteria.es"><img src="{$urls.base_url}library/images/ico-mail-header.svg"></a>
              <a href="tel:694904053"><img src="{$urls.base_url}library/images/ico-tel-header.svg"></a>
          </div>

          <article class="logo-mobile">
            <a href="index.php" title="logo">
			  <img src="{$urls.base_url}library/images/logo.svg">
            </a>
          </article>

          <div class="mobile-menu-right">
            <button calss="header-search" id="header-search"><img src="{$urls.base_url}library/images/ico-search-mobile.svg"></button>
            <article class="btn-menu">
              <span></span>
              <span></span>
              <span></span>
            </article>
          </div>

        </div>
      </section>

			<div class="container cont-1300">
				<a href="index.php" id="logo">
					<img src="{$urls.base_url}library/images/logo.svg">
				</a>
				<div id="nav-menu">
					<ul>
						<li>
							<a href="categoria.php">CONSUMIBLES</a>
							<ul class="submenu">
								<li><a class="active" href="">FIJACIÓN Y ANCLAJES</a></li>
								<li><a href="">CINTAS Y ADHESIVOS</a></li>
								<li><a href="">PINTURAS Y ACEITES</a></li>
								<li><a href="">CUERDAS Y CINTAS</a></li>

								<img src="{$urls.base_url}library/images/sombra-submenu.svg" class="sombra">
								<img src="{$urls.base_url}library/images/tornillo-submenu.png" class="tornillo">
							</ul>
						</li>
						<li><a href="categoria.php">MAQUINARIA</a></li>
						<li>
							<a href="categoria.php">HERRAMIENTAS</a>
							<ul class="submenu">
								<li><a class="active" href="">FIJACIÓN Y ANCLAJES</a></li>
								<li><a href="">CINTAS Y ADHESIVOS</a></li>
								<li><a href="">PINTURAS Y ACEITES</a></li>
								<li><a href="">CUERDAS Y CINTAS</a></li>

								<img src="{$urls.base_url}library/images/sombra-submenu.svg" class="sombra">
								<img src="{$urls.base_url}library/images/tornillo-submenu.png" class="tornillo">
							</ul>
						</li>
						<li><a href="page-contacto.php">CONTACTO</a></li>
					</ul>

					<div class="menu-user">
						<a href="" class="ico-user"><img src="{$urls.base_url}library/images/ico-user.svg"></a>
						<a href="" class="cart-header">
							<img src="{$urls.base_url}library/images/ico-cart.svg">
							<span>15</span>
						</a>
						<ul class="submenu">
							<li><a class="active" href="">REGISTRARSE</a></li>
							<li><a href="">LOGIN</a></li>
						</ul>
					</div>

					<div class="header-search">
						<input type="text" id="header-search" onchange="textoVacio()" onkeydown="buscar();" class="active" placeholder="Buscar…">
					</div>
				</div>
			</div>
		</div>
	</header>


<!-- BUSCADOR -->
<div class="search-back" style="display: none;"></div>
<div id="search" class="search" style="display: none;">
	<div class="search__filters">
		<div class="search__filters__container">
			<span class="title">FILTROS</span>

    		<input type="text" class="js-range-slider" name="my_range" value="" />

			<div class="filters-selects">
				<div class="style-select">
					<select name="" id="">
						<option value="">Marcas</option>
						<option value="">Marcas</option>
						<option value="">Marcas</option>
						<option value="">Marcas</option>
					</select>
				</div>

				<div class="style-select">
					<select name="" id="">
						<option value="">Consumibles</option>
						<option value="">Consumibles</option>
						<option value="">Consumibles</option>
						<option value="">Consumibles</option>
					</select>
				</div>

				<div class="style-select">
					<select name="" id="">
						<option value="">Maquinaria</option>
						<option value="">Maquinaria</option>
						<option value="">Maquinaria</option>
						<option value="">Maquinaria</option>
					</select>
				</div>
				<div class="style-select">
					<select name="" id="">
						<option value="">Herramientas</option>
						<option value="">Herramientas</option>
						<option value="">Herramientas</option>
						<option value="">Herramientas</option>
					</select>
				</div>
			</div>

			<button class="close-search">
				<img src="{$urls.base_url}library/images/ico-close-search.svg">
			</button>
		</div>
	</div>

	<div class="search__content">
		<span class="search__content__title">47 Resultados encontrados</span>

		<div class="search__content__box">
			<div class="search__content__list">
				<div class="o-products">
					<a href="" class="o-products__img">
						<img src="{$urls.base_url}library/images/products-1.png" alt="Titulo Producto">
					</a>
					<div class="o-products__info">
						<span class="o-products__category">Maquinaria</span>
						<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
						<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
					</div>

					<!-- Caja flotante oculta con la descripcion y botones de compra -->
					<div class="o-products__desc">
						<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
					</div>
				</div>

				<div class="o-products">
					<a href="" class="o-products__img">
						<img src="{$urls.base_url}library/images/products-1.png" alt="Titulo Producto">
					</a>
					<div class="o-products__info">
						<span class="o-products__category">Maquinaria</span>
						<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
						<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
					</div>

					<!-- Caja flotante oculta con la descripcion y botones de compra -->
					<div class="o-products__desc">
						<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
					</div>
				</div>

				<div class="o-products">
					<a href="" class="o-products__img">
						<img src="{$urls.base_url}library/images/products-1.png" alt="Titulo Producto">
					</a>
					<div class="o-products__info">
						<span class="o-products__category">Maquinaria</span>
						<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
						<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
					</div>

					<!-- Caja flotante oculta con la descripcion y botones de compra -->
					<div class="o-products__desc">
						<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
					</div>
				</div>

				<div class="o-products">
					<a href="" class="o-products__img">
						<img src="{$urls.base_url}library/images/products-1.png" alt="Titulo Producto">
					</a>
					<div class="o-products__info">
						<span class="o-products__category">Maquinaria</span>
						<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
						<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
					</div>

					<!-- Caja flotante oculta con la descripcion y botones de compra -->
					<div class="o-products__desc">
						<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
					</div>
				</div>
				<div class="o-products">
					<a href="" class="o-products__img">
						<img src="{$urls.base_url}library/images/products-1.png" alt="Titulo Producto">
					</a>
					<div class="o-products__info">
						<span class="o-products__category">Maquinaria</span>
						<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
						<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
					</div>

					<!-- Caja flotante oculta con la descripcion y botones de compra -->
					<div class="o-products__desc">
						<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
					</div>
				</div>

				<div class="o-products">
					<a href="" class="o-products__img">
						<img src="{$urls.base_url}library/images/products-1.png" alt="Titulo Producto">
					</a>
					<div class="o-products__info">
						<span class="o-products__category">Maquinaria</span>
						<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
						<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
					</div>

					<!-- Caja flotante oculta con la descripcion y botones de compra -->
					<div class="o-products__desc">
						<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
					</div>
				</div>

				<div class="o-products">
					<a href="" class="o-products__img">
						<img src="{$urls.base_url}library/images/products-1.png" alt="Titulo Producto">
					</a>
					<div class="o-products__info">
						<span class="o-products__category">Maquinaria</span>
						<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
						<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
					</div>

					<!-- Caja flotante oculta con la descripcion y botones de compra -->
					<div class="o-products__desc">
						<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
					</div>
				</div>

				<div class="o-products">
					<a href="" class="o-products__img">
						<img src="{$urls.base_url}library/images/products-1.png" alt="Titulo Producto">
					</a>
					<div class="o-products__info">
						<span class="o-products__category">Maquinaria</span>
						<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
						<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
					</div>

					<!-- Caja flotante oculta con la descripcion y botones de compra -->
					<div class="o-products__desc">
						<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>