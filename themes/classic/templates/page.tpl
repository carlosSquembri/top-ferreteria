{**
 * 2007-2019 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2019 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file=$layout}


{if $urls.current_url=="http://localhost/top-ferreteria/contacto"}
  {block name='content'}
    <main class="c-contacto">
	
      <div class="o-contacto-head">
        <div class="container-portada">
          <div class="o-contacto-head__box">
            <h2 class="o-contacto-head__subtitle">¿TIENES ALGUNA DUDA?</h2>
            <h1 class="o-contacto-head__title">Contacto y Faq’s</h1>
          </div>
        </div>
      </div>

      <section class="o-contacto">
        <div class="container cont-1300">
          <div class="o-contacto__box">
            <div class="o-contacto__box__top">
              <button id="open-contact" class="active">Formulario de contacto</button>
              <button id="open-faqs">Preguntas frecuentes</button>
            </div>
            
            <div class="o-contacto-form">
              <form action="">
                <div class="grid-form-3">
                  <label>
                    Nombre
                    <input type="text">
                  </label>
                  <label>
                    Teléfono
                    <input type="tel">
                  </label>
                  <label>
                    E-mail
                    <input type="email">
                  </label>
                </div>

                <label>
                  Comentarios
                  <textarea name="" id="" cols="30" rows="10"></textarea>
                </label>
                
                <div class="form-flex">
                  <label class="acceptance-politicas">
                    <input type="checkbox">
                    He leído y acepto los <a href="">términos y condiciones</a>
                  </label>
                  <input type="submit" value="Enviar">
                </div>
              </form>
            </div>

            <div class="o-contacto-faqs" style="display: none;">
              <button class="accordion">Duis vestibulum elit vel neque pharetra vulputate. Quisque scelerisque nisi urna. Duis rutrum non risus in imperdiet. Proin molestie accumsan nulla sit amet mattis. Ut vel tristique neque.</button>
              <div class="panel">
                <p>Curabitur sit amet eros eu arcu consectetur pulvinar. Aliquam sodales, turpis eget tristique tempor, sapien lacus facilisis diam, molestie efficitur sapien velit nec magna. Maecenas interdum efficitur tempor. Quisque scelerisque id odio nec dictum. Donec sed pulvinar tortor. Duis ut dolor consectetur, mollis lorem et, mattis est.</p>
                <p>Maecenas interdum efficitur tempor. Quisque scelerisque id odio nec dictum. Donec sed pulvinar tortor. Duis ut dolor consectetur, mollis lorem et, mattis est.</p>
              </div>

              <button class="accordion">Proin ex ipsum, facilisis id tincidunt sed, vulputate in lacus. Donec pharetra faucibus leo, vitae vestibulum2</button>
              <div class="panel">
                <p>Curabitur sit amet eros eu arcu consectetur pulvinar. Aliquam sodales, turpis eget tristique tempor, sapien lacus facilisis diam, molestie efficitur sapien velit nec magna. Maecenas interdum efficitur tempor. Quisque scelerisque id odio nec dictum. Donec sed pulvinar tortor. Duis ut dolor consectetur, mollis lorem et, mattis est.</p>
              </div>

              <button class="accordion">Integer ac interdum lacus. Nunc porta semper lacus a varius. Pellentesque habitant morbi tristique</button>
              <div class="panel">
                <p>Curabitur sit amet eros eu arcu consectetur pulvinar. Aliquam sodales, turpis eget tristique tempor, sapien lacus facilisis diam, molestie efficitur sapien velit nec magna. Maecenas interdum efficitur tempor. Quisque scelerisque id odio nec dictum. Donec sed pulvinar tortor. Duis ut dolor consectetur, mollis lorem et, mattis est.</p>
              </div>
            </div>
            
          </div>
        </div>
      </section>

    </main>
  {/block}
{else}

  {block name='content'}
    <section id="main">

      {block name='page_header_container'}
        {block name='page_title' hide}
          <header class="page-header">
            <h1>{$smarty.block.child}</h1>
          </header>
        {/block}
      {/block}

      {block name='page_content_container'}
        <section id="content" class="page-content card card-block">
          {block name='page_content_top'}{/block}
          {block name='page_content'}
            <!-- Page content -->
          {/block}
        </section>
      {/block}

      {block name='page_footer_container'}
        <!--<footer class="page-footer">-->
          {block name='page_footer'}
            <!-- Footer content -->
          {/block}
        <!--</footer>-->
      {/block}

    </section>

  {/block}

{/if}
