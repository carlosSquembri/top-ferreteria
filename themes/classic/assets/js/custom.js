/*
 * Custom code goes here.
 * A template should always ship with an empty custom.js
 */

$('.btn-menu').on('click', function (ev) {
    ev.preventDefault();
    $("#nav-menu").toggleClass('menu-mobile-active');
    $('.btn-menu').toggleClass('btn-menu-active');
});

function ir_a(elemento) {
    var posicion = $(elemento).position().top;
    posicion = posicion - 104;
    $('html,body').animate({ scrollTop: posicion }, 700);
    return;
}

// Cada vez que se haga scroll en la página
// haremos un chequeo del estado del menú
$(window).on('scroll', function () {
    if ($(window).scrollTop() >= 90) {
        $('.c-header').addClass('menu-fijo');
    } else {
        $('.c-header').removeClass('menu-fijo');
    }
});


//desplegable descripción de la categoria
$('.btn-more').on('click', function (ev) {
    ev.preventDefault();
    $(".cont-hide").toggleClass('cont-hide-active');
    $(".btn-more").toggleClass('btn-more-active');
    $(".gradient").toggleClass('gradient-active');
});

//ABRIR TABS SINGLE PRODUCTS
$('#open-especificaciones').on('click', function (ev) {
    if ($('#open-especificaciones').hasClass('active')) {
        $("#especificaciones-info").hide('slow');
        $('#open-especificaciones').removeClass('active');
    } else {
        $('#open-especificaciones').addClass('active');
        $("#especificaciones-info").show('');
        $("#prestaciones-info").hide('');
        $("#equipo-info").hide('');
    }

});

$('#open-prestaciones').on('click', function (ev) {

    if ($('#open-prestaciones').hasClass('active')) {
        $("#prestaciones-info").hide('slow');
        $('#open-prestaciones').removeClass('active');
    } else {
        $('#open-prestaciones').addClass('active');
        $("#especificaciones-info").hide('');
        $("#prestaciones-info").show('');
        $("#equipo-info").hide('');
    }
});

$('#open-equipo').on('click', function (ev) {
    if ($('#open-equipo').hasClass('active')) {
        $("#equipo-info").hide('slow');
        $('#open-equipo').removeClass('active');
    } else {
        $('#open-equipo').addClass('active');
        $("#especificaciones-info").hide('');
        $("#prestaciones-info").hide('');
        $("#equipo-info").show('');
    }
});

// ACCORDEON FAQS
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function () {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.maxHeight) {
            panel.style.maxHeight = null;
        } else {
            panel.style.maxHeight = panel.scrollHeight + "px";
        }
    });
}

// ABRIR CONTACTO O FAQS
$('#open-contact').on('click', function (ev) {
    $('#open-contact').addClass('active');
    $('#open-faqs').removeClass('active');

    $('.o-contacto-form').show();
    $('.o-contacto-faqs').hide();
});
$('#open-faqs').on('click', function (ev) {
    $('#open-contact').removeClass('active');
    $('#open-faqs').addClass('active');

    $('.o-contacto-form').hide();
    $('.o-contacto-faqs').show();
});

// BUSCADOR
$('.close-search').on('click', function (ev) {
    $('#search').hide();
    $('.search-back').hide();
    $('.c-header').removeClass('search-active');
});

// ABRIR BUSCADOR
$('#header-search').on('click', function (ev) {
    $('#search').show();
    $('.c-header').addClass('search-active');
    $('.search-back').show();
});

// ABRIR FILTROS EN MOVIL
$('.btn-open-filters').on('click', function (ev) {
    $('.o-categoria__sidebar').toggleClass('sidebar-active');
});
// CERRAR FILTROS EN MOVIL
$('.close-sidebar').on('click', function (ev) {
    $('.o-categoria__sidebar').toggleClass('sidebar-active');
});