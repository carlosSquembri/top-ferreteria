<?php
/* Smarty version 3.1.33, created on 2020-01-07 13:17:48
  from '/Applications/MAMP/htdocs/top-ferreteria/themes/classic/templates/page.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e1476ec19eef2_01689249',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f5dff4acb590db4a380e88c6e632d574c40208e3' => 
    array (
      0 => '/Applications/MAMP/htdocs/top-ferreteria/themes/classic/templates/page.tpl',
      1 => 1578398064,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e1476ec19eef2_01689249 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>



<?php if ($_smarty_tpl->tpl_vars['urls']->value['current_url'] == "http://localhost/top-ferreteria/contacto") {?>
  <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_18098851025e1476ec165b88_42919769', 'content');
?>

<?php } else { ?>

  <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_11887932085e1476ec16ba11_91380690', 'content');
?>


<?php }
$_smarty_tpl->inheritance->endChild($_smarty_tpl, $_smarty_tpl->tpl_vars['layout']->value);
}
/* {block 'content'} */
class Block_18098851025e1476ec165b88_42919769 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_18098851025e1476ec165b88_42919769',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <main class="c-contacto">
	
      <div class="o-contacto-head">
        <div class="container-portada">
          <div class="o-contacto-head__box">
            <h2 class="o-contacto-head__subtitle">¿TIENES ALGUNA DUDA?</h2>
            <h1 class="o-contacto-head__title">Contacto y Faq’s</h1>
          </div>
        </div>
      </div>

      <section class="o-contacto">
        <div class="container cont-1300">
          <div class="o-contacto__box">
            <div class="o-contacto__box__top">
              <button id="open-contact" class="active">Formulario de contacto</button>
              <button id="open-faqs">Preguntas frecuentes</button>
            </div>
            
            <div class="o-contacto-form">
              <form action="">
                <div class="grid-form-3">
                  <label>
                    Nombre
                    <input type="text">
                  </label>
                  <label>
                    Teléfono
                    <input type="tel">
                  </label>
                  <label>
                    E-mail
                    <input type="email">
                  </label>
                </div>

                <label>
                  Comentarios
                  <textarea name="" id="" cols="30" rows="10"></textarea>
                </label>
                
                <div class="form-flex">
                  <label class="acceptance-politicas">
                    <input type="checkbox">
                    He leído y acepto los <a href="">términos y condiciones</a>
                  </label>
                  <input type="submit" value="Enviar">
                </div>
              </form>
            </div>

            <div class="o-contacto-faqs" style="display: none;">
              <button class="accordion">Duis vestibulum elit vel neque pharetra vulputate. Quisque scelerisque nisi urna. Duis rutrum non risus in imperdiet. Proin molestie accumsan nulla sit amet mattis. Ut vel tristique neque.</button>
              <div class="panel">
                <p>Curabitur sit amet eros eu arcu consectetur pulvinar. Aliquam sodales, turpis eget tristique tempor, sapien lacus facilisis diam, molestie efficitur sapien velit nec magna. Maecenas interdum efficitur tempor. Quisque scelerisque id odio nec dictum. Donec sed pulvinar tortor. Duis ut dolor consectetur, mollis lorem et, mattis est.</p>
                <p>Maecenas interdum efficitur tempor. Quisque scelerisque id odio nec dictum. Donec sed pulvinar tortor. Duis ut dolor consectetur, mollis lorem et, mattis est.</p>
              </div>

              <button class="accordion">Proin ex ipsum, facilisis id tincidunt sed, vulputate in lacus. Donec pharetra faucibus leo, vitae vestibulum2</button>
              <div class="panel">
                <p>Curabitur sit amet eros eu arcu consectetur pulvinar. Aliquam sodales, turpis eget tristique tempor, sapien lacus facilisis diam, molestie efficitur sapien velit nec magna. Maecenas interdum efficitur tempor. Quisque scelerisque id odio nec dictum. Donec sed pulvinar tortor. Duis ut dolor consectetur, mollis lorem et, mattis est.</p>
              </div>

              <button class="accordion">Integer ac interdum lacus. Nunc porta semper lacus a varius. Pellentesque habitant morbi tristique</button>
              <div class="panel">
                <p>Curabitur sit amet eros eu arcu consectetur pulvinar. Aliquam sodales, turpis eget tristique tempor, sapien lacus facilisis diam, molestie efficitur sapien velit nec magna. Maecenas interdum efficitur tempor. Quisque scelerisque id odio nec dictum. Donec sed pulvinar tortor. Duis ut dolor consectetur, mollis lorem et, mattis est.</p>
              </div>
            </div>
            
          </div>
        </div>
      </section>

    </main>
  <?php
}
}
/* {/block 'content'} */
/* {block 'page_title'} */
class Block_9437783255e1476ec172125_20031423 extends Smarty_Internal_Block
{
public $callsChild = 'true';
public $hide = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <header class="page-header">
            <h1><?php 
$_smarty_tpl->inheritance->callChild($_smarty_tpl, $this);
?>
</h1>
          </header>
        <?php
}
}
/* {/block 'page_title'} */
/* {block 'page_header_container'} */
class Block_4235846595e1476ec16e137_27702766 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9437783255e1476ec172125_20031423', 'page_title', $this->tplIndex);
?>

      <?php
}
}
/* {/block 'page_header_container'} */
/* {block 'page_content_top'} */
class Block_2693926185e1476ec1809d0_20373880 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'page_content_top'} */
/* {block 'page_content'} */
class Block_305030805e1476ec185258_32650877 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <!-- Page content -->
          <?php
}
}
/* {/block 'page_content'} */
/* {block 'page_content_container'} */
class Block_943620915e1476ec17e2e1_93727020 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <section id="content" class="page-content card card-block">
          <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2693926185e1476ec1809d0_20373880', 'page_content_top', $this->tplIndex);
?>

          <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_305030805e1476ec185258_32650877', 'page_content', $this->tplIndex);
?>

        </section>
      <?php
}
}
/* {/block 'page_content_container'} */
/* {block 'page_footer'} */
class Block_2396747165e1476ec197270_51687699 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <!-- Footer content -->
          <?php
}
}
/* {/block 'page_footer'} */
/* {block 'page_footer_container'} */
class Block_9311663255e1476ec194886_43651239 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <!--<footer class="page-footer">-->
          <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2396747165e1476ec197270_51687699', 'page_footer', $this->tplIndex);
?>

        <!--</footer>-->
      <?php
}
}
/* {/block 'page_footer_container'} */
/* {block 'content'} */
class Block_11887932085e1476ec16ba11_91380690 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_11887932085e1476ec16ba11_91380690',
  ),
  'page_header_container' => 
  array (
    0 => 'Block_4235846595e1476ec16e137_27702766',
  ),
  'page_title' => 
  array (
    0 => 'Block_9437783255e1476ec172125_20031423',
  ),
  'page_content_container' => 
  array (
    0 => 'Block_943620915e1476ec17e2e1_93727020',
  ),
  'page_content_top' => 
  array (
    0 => 'Block_2693926185e1476ec1809d0_20373880',
  ),
  'page_content' => 
  array (
    0 => 'Block_305030805e1476ec185258_32650877',
  ),
  'page_footer_container' => 
  array (
    0 => 'Block_9311663255e1476ec194886_43651239',
  ),
  'page_footer' => 
  array (
    0 => 'Block_2396747165e1476ec197270_51687699',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <section id="main">

      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_4235846595e1476ec16e137_27702766', 'page_header_container', $this->tplIndex);
?>


      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_943620915e1476ec17e2e1_93727020', 'page_content_container', $this->tplIndex);
?>


      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9311663255e1476ec194886_43651239', 'page_footer_container', $this->tplIndex);
?>


    </section>

  <?php
}
}
/* {/block 'content'} */
}
