<?php
/* Smarty version 3.1.33, created on 2020-01-07 13:17:48
  from '/Applications/MAMP/htdocs/top-ferreteria/themes/classic/templates/index.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e1476ec13a1e2_73268130',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9fe937753966b119961ac43d41807d5a02c11de6' => 
    array (
      0 => '/Applications/MAMP/htdocs/top-ferreteria/themes/classic/templates/index.tpl',
      1 => 1578386784,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e1476ec13a1e2_73268130 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_15318984845e1476ec114052_84735848', 'page_content_container');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, 'page.tpl');
}
/* {block 'page_content_top'} */
class Block_16173157585e1476ec1169e6_57610894 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'page_content_top'} */
/* {block 'hook_home'} */
class Block_20277239785e1476ec11c688_36984496 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <?php echo $_smarty_tpl->tpl_vars['HOOK_HOME']->value;?>

          <?php
}
}
/* {/block 'hook_home'} */
/* {block 'page_content'} */
class Block_21082484695e1476ec11a1f6_00994023 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_20277239785e1476ec11c688_36984496', 'hook_home', $this->tplIndex);
?>

        <?php
}
}
/* {/block 'page_content'} */
/* {block 'page_content_container'} */
class Block_15318984845e1476ec114052_84735848 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'page_content_container' => 
  array (
    0 => 'Block_15318984845e1476ec114052_84735848',
  ),
  'page_content_top' => 
  array (
    0 => 'Block_16173157585e1476ec1169e6_57610894',
  ),
  'page_content' => 
  array (
    0 => 'Block_21082484695e1476ec11a1f6_00994023',
  ),
  'hook_home' => 
  array (
    0 => 'Block_20277239785e1476ec11c688_36984496',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <section id="content" class="page-home">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_16173157585e1476ec1169e6_57610894', 'page_content_top', $this->tplIndex);
?>


        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_21082484695e1476ec11a1f6_00994023', 'page_content', $this->tplIndex);
?>

      </section>
    <?php
}
}
/* {/block 'page_content_container'} */
}
