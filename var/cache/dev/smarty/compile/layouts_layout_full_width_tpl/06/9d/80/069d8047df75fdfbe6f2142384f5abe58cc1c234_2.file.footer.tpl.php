<?php
/* Smarty version 3.1.33, created on 2020-01-07 13:23:31
  from '/Applications/MAMP/htdocs/top-ferreteria/themes/classic/templates/_partials/footer.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e147843261e05_10749825',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '069d8047df75fdfbe6f2142384f5abe58cc1c234' => 
    array (
      0 => '/Applications/MAMP/htdocs/top-ferreteria/themes/classic/templates/_partials/footer.tpl',
      1 => 1578397657,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e147843261e05_10749825 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="container cont-1300">
	<div class="o-footer-top">
		<div class="o-footer-top__categorys">
			<h3>Categorías:</h3>
			<ul>
				<li><a href="">Fijación y anclajes</a></li>
				<li><a href="">Herramienta eléctrica</a></li>
				<li><a href="">Corte taladro y cepillería</a></li>
				<li><a href="">Protección laboral y seguridad</a></li>
				<li><a href="">Pinceles y aceites</a></li>
				<li><a href="">Cintas y adhesivos</a></li>
				<li><a href="">Herramienta manual</a></li>
				<li><a href="">Escaleras</a></li>
				<li><a href="">Herramienta taller</a></li>
				<li><a href="">Soldadura</a></li>
				<li><a href="">Medición y escritura</a></li>
				<li><a href="">Cuerdas y cintas</a></li>
			</ul>
		</div>
		<div class="o-footer-top__info">
			<div class="info-top">
				<a href="mailto:info@topferreteria.es"><img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['base_url'], ENT_QUOTES, 'UTF-8');?>
library/images/ico-mail-header.svg">info@topferreteria.es</a>
				<a href="tel:+ 34 694 904 053"><img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['base_url'], ENT_QUOTES, 'UTF-8');?>
library/images/ico-tel-header.svg">+ 34 694 904 053</a>
			</div>
			<div class="info-pay">
				<span>Formas de pago:</span>
				<img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['base_url'], ENT_QUOTES, 'UTF-8');?>
library/images/formas-pago.png" alt="Formas de pago">
			</div>
		</div>
	</div>
	<div class="sombra">
		<img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['base_url'], ENT_QUOTES, 'UTF-8');?>
library/images/sombra.svg">
	</div>
	<div class="o-footer-bottom">
		<a href="" id="logo-footer"><img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['base_url'], ENT_QUOTES, 'UTF-8');?>
library/images/logo-footer.svg"></a>
		<ul class="politicas">
			<li><a href="">Aviso legal</a></li>
			<li><span>|</span></li>
			<li><a href="">Política de privacidad</a></li>
			<li><span>|</span></li>
			<li><a href="">Política de cookies</a></li>
		</ul>
	</div>
</div>


<?php echo '<script'; ?>
>
	// SLIDER HOME CABECERA
	$('.o-home-slider').slick({
		arrows: false,
		infinite: false,
		slidesToShow: 1,
		slidesToScroll: 1,
		dots: true,
	});

	// SLIDER PRODUCTOS HOME
	$('.o-featured-products__list').slick({
		arrows: true,
		infinite: false,
		slidesToShow: 4,
		slidesToScroll: 1,
		dots: false,
		slidesPerRow: 1,
		rows: 2,
		responsive: [
			{
				breakpoint: 1200,
				settings: {
					slidesToShow: 3,
				}
			},
			{
				breakpoint: 991,
				settings: {
					slidesToShow: 2,
				}
			},
			{
				breakpoint: 750,
				settings: {
					slidesToShow: 1,
					rows: 4,
					slidesPerRow: 1,
				}
			},
		]
	});

	// SLIDER MARCAS
	$('.o-marcas__slider').slick({
		arrows: true,
		infinite: false,
		slidesToShow: 5,
		slidesToScroll: 1,
		dots: false,
		responsive: [
			{
				breakpoint: 991,
				settings: {
					slidesToShow: 4,
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 2,
				}
			},
			{
				breakpoint: 550,
				settings: {
					slidesToShow: 1,
				}
			},
		]
	});
	$('.slider-productos-left').slick({
		dots: false,
		infinite: false,
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		asNavFor: '.slider-productos-right',
	});
	$('.slider-productos-right').slick({
		dots: false,
		infinite: false,
		slidesToShow: 3,
		slidesToScroll: 1,
		arrows: false,
		vertical: true,
		verticalSwiping: true,
		asNavFor: '.slider-productos-left',
		responsive: [
			{
				breakpoint: 991,
				settings: {
					vertical: false,
					verticalSwiping: false,
				}
			},
			{
				breakpoint: 768,
				settings: {
					vertical: false,
					verticalSwiping: false,
					slidesToShow: 2,
					slidesToScroll: 1,
				}
			},
		]
	});
	$(".js-range-slider").ionRangeSlider({
		type: "double",
		polyfill: false,
		min: 0,
		max: 350,
		from: 20,
		to: 100,
		grid: true
	});
<?php echo '</script'; ?>
>
<?php }
}
