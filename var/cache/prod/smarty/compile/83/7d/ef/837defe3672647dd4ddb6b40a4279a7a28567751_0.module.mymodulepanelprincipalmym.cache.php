<?php
/* Smarty version 3.1.33, created on 2020-01-07 10:31:36
  from 'module:mymodulepanelprincipalmym' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e144ff8642940_37241079',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '837defe3672647dd4ddb6b40a4279a7a28567751' => 
    array (
      0 => 'module:mymodulepanelprincipalmym',
      1 => 1556032454,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e144ff8642940_37241079 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
$_smarty_tpl->compiled->nocache_hash = '16420174045e144ff8616692_56535054';
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_13989410705e144ff86224b6_75130970', 'my_modulepanelprincipal');
?>


<?php }
/* {block 'my_modulepanelprincipal'} */
class Block_13989410705e144ff86224b6_75130970 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'my_modulepanelprincipal' => 
  array (
    0 => 'Block_13989410705e144ff86224b6_75130970',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

  <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['datos']->value, 'c');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['c']->value) {
?>
    <section class="c-promo-home">
      <article class="o-img">
        <img src="<?php echo $_smarty_tpl->tpl_vars['urls']->value['base_url'];?>
img/imagenesslider/<?php echo $_smarty_tpl->tpl_vars['c']->value['imagen'];?>
" alt="Imagen Promo">
      </article>
      <article class="o-text">
        <div class="o-text__cont">
          <h2><?php echo $_smarty_tpl->tpl_vars['c']->value['titulo'];?>
</h2>
          <p><?php echo $_smarty_tpl->tpl_vars['c']->value['descripcion'];?>
</p>
          <span class="o-text__desc"><?php echo $_smarty_tpl->tpl_vars['c']->value['precio'];?>
</span>
          <p class="o-text__more">
            <a href="<?php echo $_smarty_tpl->tpl_vars['c']->value['enlace'];?>
" title="Lo quiero">¡Lo quiero!</a>
          </p>
        </div>
      </article>
    </section>
  <?php ob_start();
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
$_prefixVariable1 = ob_get_clean();
echo $_prefixVariable1;?>

<?php
}
}
/* {/block 'my_modulepanelprincipal'} */
}
