<?php
/* Smarty version 3.1.33, created on 2020-01-07 11:51:11
  from '/Applications/MAMP/htdocs/top-ferreteria/themes/classic/templates/page.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e14629f4ddb27_13545707',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f5dff4acb590db4a380e88c6e632d574c40208e3' => 
    array (
      0 => '/Applications/MAMP/htdocs/top-ferreteria/themes/classic/templates/page.tpl',
      1 => 1578386784,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e14629f4ddb27_13545707 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_15677712145e14629f4aadf9_04139651', 'content');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, $_smarty_tpl->tpl_vars['layout']->value);
}
/* {block 'page_title'} */
class Block_5422684765e14629f4b0080_64171889 extends Smarty_Internal_Block
{
public $callsChild = 'true';
public $hide = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <header class="page-header">
          <h1><?php 
$_smarty_tpl->inheritance->callChild($_smarty_tpl, $this);
?>
</h1>
        </header>
      <?php
}
}
/* {/block 'page_title'} */
/* {block 'page_header_container'} */
class Block_10134804765e14629f4ac9e9_67674479 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_5422684765e14629f4b0080_64171889', 'page_title', $this->tplIndex);
?>

    <?php
}
}
/* {/block 'page_header_container'} */
/* {block 'page_content_top'} */
class Block_21396812675e14629f4bbac1_52025146 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'page_content_top'} */
/* {block 'page_content'} */
class Block_4878540195e14629f4c3860_75478636 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <!-- Page content -->
        <?php
}
}
/* {/block 'page_content'} */
/* {block 'page_content_container'} */
class Block_1326285495e14629f4b9591_81952767 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <section id="content" class="page-content card card-block">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_21396812675e14629f4bbac1_52025146', 'page_content_top', $this->tplIndex);
?>

        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_4878540195e14629f4c3860_75478636', 'page_content', $this->tplIndex);
?>

      </section>
    <?php
}
}
/* {/block 'page_content_container'} */
/* {block 'page_footer'} */
class Block_11363275295e14629f4d8bc8_08980335 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <!-- Footer content -->
        <?php
}
}
/* {/block 'page_footer'} */
/* {block 'page_footer_container'} */
class Block_15146454305e14629f4d62d1_39584644 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <footer class="page-footer">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_11363275295e14629f4d8bc8_08980335', 'page_footer', $this->tplIndex);
?>

      </footer>
    <?php
}
}
/* {/block 'page_footer_container'} */
/* {block 'content'} */
class Block_15677712145e14629f4aadf9_04139651 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_15677712145e14629f4aadf9_04139651',
  ),
  'page_header_container' => 
  array (
    0 => 'Block_10134804765e14629f4ac9e9_67674479',
  ),
  'page_title' => 
  array (
    0 => 'Block_5422684765e14629f4b0080_64171889',
  ),
  'page_content_container' => 
  array (
    0 => 'Block_1326285495e14629f4b9591_81952767',
  ),
  'page_content_top' => 
  array (
    0 => 'Block_21396812675e14629f4bbac1_52025146',
  ),
  'page_content' => 
  array (
    0 => 'Block_4878540195e14629f4c3860_75478636',
  ),
  'page_footer_container' => 
  array (
    0 => 'Block_15146454305e14629f4d62d1_39584644',
  ),
  'page_footer' => 
  array (
    0 => 'Block_11363275295e14629f4d8bc8_08980335',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


  <section id="main">

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10134804765e14629f4ac9e9_67674479', 'page_header_container', $this->tplIndex);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1326285495e14629f4b9591_81952767', 'page_content_container', $this->tplIndex);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_15146454305e14629f4d62d1_39584644', 'page_footer_container', $this->tplIndex);
?>


  </section>

<?php
}
}
/* {/block 'content'} */
}
