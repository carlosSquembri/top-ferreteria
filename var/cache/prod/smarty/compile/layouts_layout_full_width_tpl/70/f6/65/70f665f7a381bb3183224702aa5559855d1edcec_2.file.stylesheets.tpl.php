<?php
/* Smarty version 3.1.33, created on 2020-01-07 11:51:11
  from '/Applications/MAMP/htdocs/top-ferreteria/themes/classic/templates/_partials/stylesheets.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e14629f806b91_89797070',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '70f665f7a381bb3183224702aa5559855d1edcec' => 
    array (
      0 => '/Applications/MAMP/htdocs/top-ferreteria/themes/classic/templates/_partials/stylesheets.tpl',
      1 => 1578393171,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e14629f806b91_89797070 (Smarty_Internal_Template $_smarty_tpl) {
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['stylesheets']->value['external'], 'stylesheet');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['stylesheet']->value) {
?>
  <?php if ($_smarty_tpl->tpl_vars['stylesheet']->value['uri'] != ((string)$_smarty_tpl->tpl_vars['urls']->value['base_url'])."themes/classic/assets/css/theme.css") {?>
    <link rel="stylesheet" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['stylesheet']->value['uri'], ENT_QUOTES, 'UTF-8');?>
" type="text/css" media="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['stylesheet']->value['media'], ENT_QUOTES, 'UTF-8');?>
">
  <?php }
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

<?php if ((($_smarty_tpl->tpl_vars['page']->value['page_name'] == "cart") || ($_smarty_tpl->tpl_vars['page']->value['page_name'] == "module-my_moduleproductofavorito-account") || ($_smarty_tpl->tpl_vars['page']->value['page_name'] == "checkout") || ($_smarty_tpl->tpl_vars['page']->value['page_name'] == "order-confirmation") || ($_smarty_tpl->tpl_vars['page']->value['page_name'] == "my-account") || ($_smarty_tpl->tpl_vars['page']->value['page_name'] == "module-ps_emailalerts-account") || ($_smarty_tpl->tpl_vars['page']->value['page_name'] == "authentication") || ($_smarty_tpl->tpl_vars['page']->value['page_name'] == "password") || ($_smarty_tpl->tpl_vars['page']->value['page_name'] == "discount") || ($_smarty_tpl->tpl_vars['page']->value['page_name'] == "module-psgdpr-gdpr") || ($_smarty_tpl->tpl_vars['page']->value['page_name'] == "identity") || ($_smarty_tpl->tpl_vars['page']->value['page_name'] == "order-detail") || ($_smarty_tpl->tpl_vars['page']->value['page_name'] == "order-slip") || ($_smarty_tpl->tpl_vars['page']->value['page_name'] == "addresses") || ($_smarty_tpl->tpl_vars['page']->value['page_name'] == "address") || ($_smarty_tpl->tpl_vars['page']->value['page_name'] == "history"))) {?>
    <link rel="stylesheet" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['base_url'], ENT_QUOTES, 'UTF-8');?>
themes/classic/assets/css/theme.css" type="text/css" media="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['stylesheet']->value['media'], ENT_QUOTES, 'UTF-8');?>
">
<?php }?>

<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['stylesheets']->value['inline'], 'stylesheet');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['stylesheet']->value) {
?>
  <style>
    <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['stylesheet']->value['content'], ENT_QUOTES, 'UTF-8');?>

  </style>
<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
}
}
