<?php
/* Smarty version 3.1.33, created on 2020-01-07 11:48:48
  from '/Applications/MAMP/htdocs/top-ferreteria/themes/classic/templates/_partials/footer.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e1462101d1584_20573253',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '069d8047df75fdfbe6f2142384f5abe58cc1c234' => 
    array (
      0 => '/Applications/MAMP/htdocs/top-ferreteria/themes/classic/templates/_partials/footer.tpl',
      1 => 1578394111,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e1462101d1584_20573253 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="container cont-1300">
	<div class="o-footer-top">
		<div class="o-footer-top__categorys">
			<h3>Categorías:</h3>
			<ul>
				<li><a href="">Fijación y anclajes</a></li>
				<li><a href="">Herramienta eléctrica</a></li>
				<li><a href="">Corte taladro y cepillería</a></li>
				<li><a href="">Protección laboral y seguridad</a></li>
				<li><a href="">Pinceles y aceites</a></li>
				<li><a href="">Cintas y adhesivos</a></li>
				<li><a href="">Herramienta manual</a></li>
				<li><a href="">Escaleras</a></li>
				<li><a href="">Herramienta taller</a></li>
				<li><a href="">Soldadura</a></li>
				<li><a href="">Medición y escritura</a></li>
				<li><a href="">Cuerdas y cintas</a></li>
			</ul>
		</div>
		<div class="o-footer-top__info">
			<div class="info-top">
				<a href="mailto:info@topferreteria.es"><?php echo '<?php ';?>svg('ico-mail-header.svg'); <?php echo '?>';?>info@topferreteria.es</a>
				<a href="tel:+ 34 694 904 053"><?php echo '<?php ';?>svg('ico-tel-header.svg'); <?php echo '?>';?>+ 34 694 904 053</a>
			</div>
			<div class="info-pay">
				<span>Formas de pago:</span>
				<img src="library/images/formas-pago.png" alt="Formas de pago">
			</div>
		</div>
	</div>
	<div class="sombra">
		<?php echo '<?php ';?>svg('sombra.svg'); <?php echo '?>';?>
	</div>
	<div class="o-footer-bottom">
		<a href="" id="logo-footer"><?php echo '<?php ';?>svg('logo-footer.svg') <?php echo '?>';?></a>
		<ul class="politicas">
			<li><a href="">Aviso legal</a></li>
			<li><span>|</span></li>
			<li><a href="">Política de privacidad</a></li>
			<li><span>|</span></li>
			<li><a href="">Política de cookies</a></li>
		</ul>
	</div>
</div>

<!-- jQuery, Bootstrap y Slick -->
<?php echo '<script'; ?>
 src="library/js/jquery.min.js"><?php echo '</script'; ?>
>
<!-- <?php echo '<script'; ?>
 src="library/js/bootstrap.min.js"><?php echo '</script'; ?>
> -->
<!-- slick -->
<?php echo '<script'; ?>
 type="text/javascript" src="library/js/slick.min.js"><?php echo '</script'; ?>
>
<!-- RANGE SLIDER -->
<?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.0/js/ion.rangeSlider.min.js"><?php echo '</script'; ?>
>

<!-- Fin jQuery, Bootstrap y Slick -->

	<?php echo '<script'; ?>
>
		$('.btn-menu').on('click', function (ev) {
	      ev.preventDefault();
	      $("#nav-menu").toggleClass('menu-mobile-active');
	      $('.btn-menu').toggleClass('btn-menu-active');
	    });

	    function ir_a(elemento){
	      var posicion = $(elemento).position().top;
	      posicion = posicion - 104;
	      $('html,body').animate({scrollTop: posicion}, 700);
	      return;
	    }

	    // Cada vez que se haga scroll en la página
	    // haremos un chequeo del estado del menú
	    $(window).on('scroll', function() {
	      if($(window).scrollTop() >= 90) {
	        $('.c-header').addClass('menu-fijo');
	      } else {
	        $('.c-header').removeClass('menu-fijo');
	      }
	    });

	    // SLIDER HOME CABECERA
	    $('.o-home-slider').slick({
		    arrows: false,
		    infinite: false,
		    slidesToShow: 1,
		    slidesToScroll: 1,
		    dots: true,
		});

	    // SLIDER PRODUCTOS HOME
		$('.o-featured-products__list').slick({
		    arrows: true,
		    infinite: false,
		    slidesToShow: 4,
		    slidesToScroll: 1,
		    dots: false,
		    slidesPerRow: 1,
    		rows: 2,
    		responsive: [
		      {
		        breakpoint: 1200,
		        settings: {
		          slidesToShow: 3,
		        }
		      },
		      {
		        breakpoint: 991,
		        settings: {
		          slidesToShow: 2,
		        }
		      },
		      {
		        breakpoint: 750,
		        settings: {
		          slidesToShow: 1,
		          rows: 4,
		          slidesPerRow: 1,
		        }
		      },
		    ]
		});

		// SLIDER MARCAS
		$('.o-marcas__slider').slick({
		    arrows: true,
		    infinite: false,
		    slidesToShow: 5,
		    slidesToScroll: 1,
		    dots: false,
		    responsive: [
		      {
		        breakpoint: 991,
		        settings: {
		          slidesToShow: 4,
		        }
		      },
		      {
		        breakpoint: 768,
		        settings: {
		          slidesToShow: 2,
		        }
		      },
		      {
		        breakpoint: 550,
		        settings: {
		          slidesToShow: 1,
		        }
		      },
		    ]
		});

		//desplegable descripción de la categoria
		$('.btn-more').on('click', function (ev) {
	      ev.preventDefault();
	        $(".cont-hide").toggleClass('cont-hide-active');
	        $(".btn-more").toggleClass('btn-more-active');
	        $(".gradient").toggleClass('gradient-active');
	    });

	    $('.slider-productos-left').slick({
		    dots: false,
		    infinite: false,
		    slidesToShow: 1,
		    slidesToScroll: 1,
		    arrows: false,
		    asNavFor: '.slider-productos-right',
		  });
		  $('.slider-productos-right').slick({
		    dots: false,
		    infinite: false,
		    slidesToShow: 3,
		    slidesToScroll: 1,
		    arrows: false,
		    vertical: true,
		    verticalSwiping: true,
		    asNavFor: '.slider-productos-left',
		    responsive: [
		      {
		        breakpoint: 991,
		        settings: {
		          vertical: false,
		          verticalSwiping: false,
		        }
		      },
		      {
		        breakpoint: 768,
		        settings: {
		          vertical: false,
		          verticalSwiping: false,
		          slidesToShow: 2,
		          slidesToScroll: 1,
		        }
		      },
		    ]
		  });

		//ABRIR TABS SINGLE PRODUCTS
	  	$('#open-especificaciones').on('click', function (ev) {
	  		if ($('#open-especificaciones').hasClass('active')) {
	  			$("#especificaciones-info").hide('slow');
	  			$('#open-especificaciones').removeClass('active');
	  		}else{
	  			$('#open-especificaciones').addClass('active');
		    	$("#especificaciones-info").show('');
		    	$("#prestaciones-info").hide('');
		    	$("#equipo-info").hide('');
	  		}
	  		
	   	});

	   	$('#open-prestaciones').on('click', function (ev) {

	   		if ($('#open-prestaciones').hasClass('active')) {
	  			$("#prestaciones-info").hide('slow');
	  			$('#open-prestaciones').removeClass('active');
	  		}else{
	  			$('#open-prestaciones').addClass('active');
		    	$("#especificaciones-info").hide('');
		    	$("#prestaciones-info").show('');
		    	$("#equipo-info").hide('');
	  		}
	   	});

	   	$('#open-equipo').on('click', function (ev) {
	    	if ($('#open-equipo').hasClass('active')) {
	  			$("#equipo-info").hide('slow');
	  			$('#open-equipo').removeClass('active');
	  		}else{
	  			$('#open-equipo').addClass('active');
		    	$("#especificaciones-info").hide('');
		    	$("#prestaciones-info").hide('');
		    	$("#equipo-info").show('');
	  		}
	   	});

	   	// ACCORDEON FAQS
	   	var acc = document.getElementsByClassName("accordion");
		var i;

		for (i = 0; i < acc.length; i++) {
		  acc[i].addEventListener("click", function() {
		    this.classList.toggle("active");
		    var panel = this.nextElementSibling;
		    if (panel.style.maxHeight) {
		      panel.style.maxHeight = null;
		    } else {
		      panel.style.maxHeight = panel.scrollHeight + "px";
		    }
		  });
		}

		// ABRIR CONTACTO O FAQS
		$('#open-contact').on('click', function (ev) {
	    	$('#open-contact').addClass('active');
	    	$('#open-faqs').removeClass('active');

	    	$('.o-contacto-form').show();
	    	$('.o-contacto-faqs').hide();
	   	});
	   	$('#open-faqs').on('click', function (ev) {
	    	$('#open-contact').removeClass('active');
	    	$('#open-faqs').addClass('active');

	    	$('.o-contacto-form').hide();
	    	$('.o-contacto-faqs').show();
	   	});

	   	// BUSCADOR
	   	$('.close-search').on('click', function (ev) {
	    	$('#search').hide();
	    	$('.search-back').hide();
	    	$('.c-header').removeClass('search-active');
	   	});

	   	$(".js-range-slider").ionRangeSlider({
	        type: "double",
	        polyfill: false,
	        min: 0,
	        max: 350,
	        from: 20,
	        to: 100,
	        grid: true
	    });

	    // ABRIR BUSCADOR
	    $('#header-search').on('click', function (ev) {
	    	$('#search').show();
	    	$('.c-header').addClass('search-active');
	    	$('.search-back').show();
	   	});

	   	// ABRIR FILTROS EN MOVIL
	    $('.btn-open-filters').on('click', function (ev) {
	    	$('.o-categoria__sidebar').toggleClass('sidebar-active');
	   	});
	   	// CERRAR FILTROS EN MOVIL
	    $('.close-sidebar').on('click', function (ev) {
	    	$('.o-categoria__sidebar').toggleClass('sidebar-active');
	   	});
		
	<?php echo '</script'; ?>
>
<?php }
}
