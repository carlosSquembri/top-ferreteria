<?php
/* Smarty version 3.1.33, created on 2020-01-07 11:51:11
  from '/Applications/MAMP/htdocs/top-ferreteria/themes/classic/templates/_partials/header.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e14629f93f683_11758968',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e406408203b994f868c8a3ccf229508794f76199' => 
    array (
      0 => '/Applications/MAMP/htdocs/top-ferreteria/themes/classic/templates/_partials/header.tpl',
      1 => 1578392473,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e14629f93f683_11758968 (Smarty_Internal_Template $_smarty_tpl) {
?>
<header class="c-header">
		<div class="header-top">
			<div class="container cont-1300">
				<span><?php echo '<?php ';?>svg('ico-monedero-header.svg'); <?php echo '?>';?>¡Precios super, super, super ajustados!</span>

				<div class="">
					<a href="mailto:info@topferreteria.es"><?php echo '<?php ';?>svg('ico-mail-header.svg'); <?php echo '?>';?>info@topferreteria.es</a>
					<a href="tel:694904053"><?php echo '<?php ';?>svg('ico-tel-header.svg'); <?php echo '?>';?>+34 694 904 053</a>
				</div>
			</div>
		</div>

		<div id="menu">
			<section class="pre-menu-mobile">
        <div class="container cont-1300">
          <div class="mobile-menu-left">
              <a href="mailto:info@topferreteria.es"><?php echo '<?php ';?>svg('ico-mail-header.svg'); <?php echo '?>';?></a>
              <a href="tel:694904053"><?php echo '<?php ';?>svg('ico-tel-header.svg'); <?php echo '?>';?></a>
          </div>

          <article class="logo-mobile">
            <a href="index.php" title="logo">
              <?php echo '<?php ';?>svg('logo.svg') <?php echo '?>';?>
            </a>
          </article>

          <div class="mobile-menu-right">
            <button calss="header-search" id="header-search"><?php echo '<?php ';?>svg('ico-search-mobile.svg'); <?php echo '?>';?></button>
            <article class="btn-menu">
              <span></span>
              <span></span>
              <span></span>
            </article>
          </div>

        </div>
      </section>

			<div class="container cont-1300">
				<a href="index.php" id="logo">
					<?php echo '<?php ';?>svg('logo.svg') <?php echo '?>';?>
				</a>
				<div id="nav-menu">
					<ul>
						<li>
							<a href="categoria.php">CONSUMIBLES</a>
							<ul class="submenu">
								<li><a class="active" href="">FIJACIÓN Y ANCLAJES</a></li>
								<li><a href="">CINTAS Y ADHESIVOS</a></li>
								<li><a href="">PINTURAS Y ACEITES</a></li>
								<li><a href="">CUERDAS Y CINTAS</a></li>

								<img src="library/images/sombra-submenu.svg" class="sombra">
								<img src="library/images/tornillo-submenu.png" class="tornillo">
							</ul>
						</li>
						<li><a href="categoria.php">MAQUINARIA</a></li>
						<li>
							<a href="categoria.php">HERRAMIENTAS</a>
							<ul class="submenu">
								<li><a class="active" href="">FIJACIÓN Y ANCLAJES</a></li>
								<li><a href="">CINTAS Y ADHESIVOS</a></li>
								<li><a href="">PINTURAS Y ACEITES</a></li>
								<li><a href="">CUERDAS Y CINTAS</a></li>

								<img src="library/images/sombra-submenu.svg" class="sombra">
								<img src="library/images/tornillo-submenu.png" class="tornillo">
							</ul>
						</li>
						<li><a href="page-contacto.php">CONTACTO</a></li>
					</ul>

					<div class="menu-user">
						<a href="" class="ico-user"><?php echo '<?php ';?>svg('ico-user.svg') <?php echo '?>';?></a>
						<a href="" class="cart-header">
							<?php echo '<?php ';?>svg('ico-cart.svg') <?php echo '?>';?>
							<span>15</span>
						</a>
						<ul class="submenu">
							<li><a class="active" href="">REGISTRARSE</a></li>
							<li><a href="">LOGIN</a></li>
						</ul>
					</div>

					<div class="header-search">
						<input type="text" id="header-search" onchange="textoVacio()" onkeydown="buscar();" class="active" placeholder="Buscar…">
					</div>
				</div>
			</div>
		</div>
	</header>


<!-- BUSCADOR -->
<div class="search-back" style="display: none;"></div>
<div id="search" class="search" style="display: none;">
	<div class="search__filters">
		<div class="search__filters__container">
			<span class="title">FILTROS</span>

    		<input type="text" class="js-range-slider" name="my_range" value="" />

			<div class="filters-selects">
				<div class="style-select">
					<select name="" id="">
						<option value="">Marcas</option>
						<option value="">Marcas</option>
						<option value="">Marcas</option>
						<option value="">Marcas</option>
					</select>
				</div>

				<div class="style-select">
					<select name="" id="">
						<option value="">Consumibles</option>
						<option value="">Consumibles</option>
						<option value="">Consumibles</option>
						<option value="">Consumibles</option>
					</select>
				</div>

				<div class="style-select">
					<select name="" id="">
						<option value="">Maquinaria</option>
						<option value="">Maquinaria</option>
						<option value="">Maquinaria</option>
						<option value="">Maquinaria</option>
					</select>
				</div>
				<div class="style-select">
					<select name="" id="">
						<option value="">Herramientas</option>
						<option value="">Herramientas</option>
						<option value="">Herramientas</option>
						<option value="">Herramientas</option>
					</select>
				</div>
			</div>

			<button class="close-search">
				<?php echo '<?php ';?>svg('ico-close-search.svg'); <?php echo '?>';?>
			</button>
		</div>
	</div>

	<div class="search__content">
		<span class="search__content__title">47 Resultados encontrados</span>

		<div class="search__content__box">
			<div class="search__content__list">
				<div class="o-products">
					<a href="" class="o-products__img">
						<img src="library/images/products-1.png" alt="Titulo Producto">
					</a>
					<div class="o-products__info">
						<span class="o-products__category">Maquinaria</span>
						<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
						<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
					</div>

					<!-- Caja flotante oculta con la descripcion y botones de compra -->
					<div class="o-products__desc">
						<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
					</div>
				</div>

				<div class="o-products">
					<a href="" class="o-products__img">
						<img src="library/images/products-1.png" alt="Titulo Producto">
					</a>
					<div class="o-products__info">
						<span class="o-products__category">Maquinaria</span>
						<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
						<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
					</div>

					<!-- Caja flotante oculta con la descripcion y botones de compra -->
					<div class="o-products__desc">
						<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
					</div>
				</div>

				<div class="o-products">
					<a href="" class="o-products__img">
						<img src="library/images/products-1.png" alt="Titulo Producto">
					</a>
					<div class="o-products__info">
						<span class="o-products__category">Maquinaria</span>
						<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
						<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
					</div>

					<!-- Caja flotante oculta con la descripcion y botones de compra -->
					<div class="o-products__desc">
						<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
					</div>
				</div>

				<div class="o-products">
					<a href="" class="o-products__img">
						<img src="library/images/products-1.png" alt="Titulo Producto">
					</a>
					<div class="o-products__info">
						<span class="o-products__category">Maquinaria</span>
						<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
						<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
					</div>

					<!-- Caja flotante oculta con la descripcion y botones de compra -->
					<div class="o-products__desc">
						<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
					</div>
				</div>
				<div class="o-products">
					<a href="" class="o-products__img">
						<img src="library/images/products-1.png" alt="Titulo Producto">
					</a>
					<div class="o-products__info">
						<span class="o-products__category">Maquinaria</span>
						<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
						<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
					</div>

					<!-- Caja flotante oculta con la descripcion y botones de compra -->
					<div class="o-products__desc">
						<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
					</div>
				</div>

				<div class="o-products">
					<a href="" class="o-products__img">
						<img src="library/images/products-1.png" alt="Titulo Producto">
					</a>
					<div class="o-products__info">
						<span class="o-products__category">Maquinaria</span>
						<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
						<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
					</div>

					<!-- Caja flotante oculta con la descripcion y botones de compra -->
					<div class="o-products__desc">
						<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
					</div>
				</div>

				<div class="o-products">
					<a href="" class="o-products__img">
						<img src="library/images/products-1.png" alt="Titulo Producto">
					</a>
					<div class="o-products__info">
						<span class="o-products__category">Maquinaria</span>
						<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
						<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
					</div>

					<!-- Caja flotante oculta con la descripcion y botones de compra -->
					<div class="o-products__desc">
						<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
					</div>
				</div>

				<div class="o-products">
					<a href="" class="o-products__img">
						<img src="library/images/products-1.png" alt="Titulo Producto">
					</a>
					<div class="o-products__info">
						<span class="o-products__category">Maquinaria</span>
						<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
						<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
					</div>

					<!-- Caja flotante oculta con la descripcion y botones de compra -->
					<div class="o-products__desc">
						<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div><?php }
}
