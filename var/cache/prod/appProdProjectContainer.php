<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\Container7saos2v\appProdProjectContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/Container7saos2v/appProdProjectContainer.php') {
    touch(__DIR__.'/Container7saos2v.legacy');

    return;
}

if (!\class_exists(appProdProjectContainer::class, false)) {
    \class_alias(\Container7saos2v\appProdProjectContainer::class, appProdProjectContainer::class, false);
}

return new \Container7saos2v\appProdProjectContainer([
    'container.build_hash' => '7saos2v',
    'container.build_id' => 'e63bb5ab',
    'container.build_time' => 1578389481,
], __DIR__.\DIRECTORY_SEPARATOR.'Container7saos2v');
