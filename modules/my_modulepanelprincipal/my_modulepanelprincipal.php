<?php
if (!defined('_PS_VERSION_')) {exit;}

use PrestaShop\PrestaShop\Core\Module\WidgetInterface;

class My_modulepanelprincipal extends Module implements WidgetInterface{

  private $templateFile;

  public function __construct() {
      $this->name = 'my_modulepanelprincipal';
      $this->tab = 'administration';
      $this->version = '1.0.0';
      $this->author = 'Squembri';
      $this->bootstrap = true;
      parent::__construct();
      $this->displayName = $this->l('Panel Principal');
      $this->description = $this->l('Gestión de la home [...]');
      $this->ps_versions_compliancy = ['min' => '1.7', 'max' => _PS_VERSION_];
      $this->templateFile = 'module:my_modulepanelprincipal/my_modulepanelprincipal.tpl';
  }

  // metodo que instala el modulo en el backoffice donde quiera

  public function installTab($parent_class, $class_name, $name)  {
    $tab = new Tab();
    // Define the title of your tab that will be displayed in BO
    $tab->name[$this->context->language->id] = $name; 
    // Name of your admin controller 
    $tab->class_name = $class_name;
    // Id of the controller where the tab will be attached
    // If you want to attach it to the root, it will be id 0 (I'll explain it below)
    $tab->id_parent = (int) Tab::getIdFromClassName($parent_class);
    // Name of your module, if you're not working in a module, just ignore it, it will be set to null in DB
    $tab->module = $this->name;
    // Other field like the position will be set to the last, if you want to put it to the top you'll have to modify the position fields directly in your DB
    return $tab->add();
  }
   
  public function install()
  {
    return (parent::install() &&
          $this->registerHook('backOfficeHeader') &&
          $this->registerHook('displayTopColumn') &&
          $this->registerHook('header') &&
          $this->registerHook('displayPanelprincipal') &&
          $this->registerHook('footer') &&
          // el primer parametro es donde aparecerá, el segundo el nombre del controlador y el tercero el titulo que aparece en el backoffice
          $this->installTab('IMPROVE', 'AdminPanelprincipal', 'Panel Principal') && Configuration::updateValue('MY_MODULEREPANELPRINCIPAL', "My_modulepanelprincipal")
    );
  }

  /*public function hookDisplayResenas($params){
    return "";
  }*/


  public function hookDisplaySliderAjax()
  {
    // Create a link with the good path
    $link = new Link;
    $parameters = array("action" => "action_name");
    $ajax_link = $link->getModuleLink('modulename','controller', $parameters);

    Media::addJsDef(array(
        "ajax_link" => $ajax_link
    ));

  }


  public function getPanelprincipal()
  {
    $consulta = 'SELECT * from ps_panelprincipal where id="1"';
    $resultado = Db::getInstance()->ExecuteS($consulta);
    return $resultado;
  }

  public function _clearCache($template, $cache_id = null, $compile_id = null)
  {
      parent::_clearCache($this->templateFile);
  }

  public function getWidgetVariables($hookName = null, array $configuration = [])
  {
    return array(
        'datos'=> "",//$this->getPanelprincipal(),
    );
  }
  public function renderWidget($hookName = null, array $configuration = [])
  {
      if (!$this->isCached($this->templateFile, $this->getCacheId('my_modulepanelprincipal'))) {
          $this->smarty->assign($this->getWidgetVariables($hookName, $configuration));
      }
      return $this->fetch($this->templateFile, $this->getCacheId('my_modulepanelprincipal'));
  }
}