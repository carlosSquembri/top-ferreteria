{block name='my_modulepanelprincipal'}

<main class="c-home">
	<div class="container-portada">
		<!-- El contenedor del slider -->
		<div class="o-home-slider">

			<!-- Cada caja del slider -->
			<div class="o-home-slider__cont">
				<img src="library/images/home-slider-1.jpg">
				<div class="container cont-1200">
					<div class="o-home-slider__cont__info">
						<h3><span>OFERTAS</span> especiales en herramientas</h3>
						<p>Descubre la amplia gama de descuentos que encontrarás en nuestra tienda</p>
					</div>
				</div>
			</div>
			<!-- // Fin ejemplo caja slider -->

			<div class="o-home-slider__cont">
				<img src="library/images/home-slider-1.jpg">
				<div class="container cont-1200">
					<div class="o-home-slider__cont__info">
						<h3><span>OFERTAS</span> especiales en herramientas</h3>
						<p>Descubre la amplia gama de descuentos que encontrarás en nuestra tienda</p>
					</div>
				</div>
			</div>
			<div class="o-home-slider__cont">
				<img src="library/images/home-slider-1.jpg">
				<div class="container cont-1200">
					<div class="o-home-slider__cont__info">
						<h3><span>OFERTAS</span> especiales en herramientas</h3>
						<p>Descubre la amplia gama de descuentos que encontrarás en nuestra tienda</p>
					</div>
				</div>
			</div>
			<div class="o-home-slider__cont">
				<img src="library/images/home-slider-1.jpg">
				<div class="container cont-1200">
					<div class="o-home-slider__cont__info">
						<h3><span>OFERTAS</span> especiales en herramientas</h3>
						<p>Descubre la amplia gama de descuentos que encontrarás en nuestra tienda</p>
					</div>
				</div>
			</div>
		</div>
		<!-- FIN CONTENEDOR SLIDER -->

		<!-- TRES BLOSQUES CON INFROMACIÓN SOBRE ENVIO FLOTANDO -->
		<div class="o-home-details">
			<div class="o-home-details__box">
				<img src="library/images/ico-envio-home.svg">
				<div>
					<h3>Envíos gratis a partir de 60€*</h3>
					<p>*Excepto en artículos pesados</p>
				</div>
			</div>
			<div class="o-home-details__box">
				<img src="library/images/ico-candado-home.svg">
				<div>
					<h3>Esta página es segura</h3>
					<p>para tus compras</p>
				</div>
			</div>
			<div class="o-home-details__box">
				<img src="library/images/ico-formas-home.svg">
				<div>
					<h3>Formas de pago disponibles</h3>
					<p>PayPal, Mastercard, Visa</p>
				</div>
			</div>
		</div>

	</div>

	<section class="o-category-home">
		<div class="container cont-1300">
			<h2 class="title-destacado"><span>CONOCE NUESTRA GAMA</span>Categorías de Productos</h2>
			<div class="o-category-home__list">
				<a class="o-category-home__list__category" href="">
					<span>FIJACIÓN Y ANCLAJES</span>
					<span class="category-eye"><img src="library/images/ico-eye.svg"></span>
				</a>
				<a class="o-category-home__list__category" href="">
					<span>CINTAS Y ADHESIVOS</span>
					<span class="category-eye"><img src="library/images/ico-eye.svg"></span>
				</a>
				<a class="o-category-home__list__category" href="">
					<span>HERRAMIENTA ELÉCTRICA</span>
					<span class="category-eye"><img src="library/images/ico-eye.svg"></span>
				</a>
				<a class="o-category-home__list__category" href="">
					<span>HERRAMIENTA MANUAL</span>
					<span class="category-eye"><img src="library/images/ico-eye.svg"></span>
				</a>
				<a class="o-category-home__list__category" href="">
					<span>CORTE, TALADRADO Y CEPILLERÍA</span>
					<span class="category-eye"><img src="library/images/ico-eye.svg"></span>
				</a>
				<a class="o-category-home__list__category" href="">
					<span>ESCALERAS</span>
					<span class="category-eye"><img src="library/images/ico-eye.svg"></span>
				</a>

				<a class="o-category-home__list__category" href="">
					<span>PROTECCION LABORAL Y SEGURIDAD</span>
					<span class="category-eye"><img src="library/images/ico-eye.svg"></span>
				</a>
				<a class="o-category-home__list__category" href="">
					<span>HERRAMIENTA TALLER</span>
					<span class="category-eye"><img src="library/images/ico-eye.svg"></span>
				</a>
				<a class="o-category-home__list__category" href="">
					<span>SOLDADURA</span>
					<span class="category-eye"><img src="library/images/ico-eye.svg"></span>
				</a>
				<a class="o-category-home__list__category" href="">
					<span>MEDICION Y ESCRITURA</span>
					<span class="category-eye"><img src="library/images/ico-eye.svg"></span>
				</a>
				<a class="o-category-home__list__category" href="">
					<span>PINTURAS Y ACEITES</span>
					<span class="category-eye"><img src="library/images/ico-eye.svg"></span>
				</a>
				<a class="o-category-home__list__category" href="">
					<span>CUERDAS Y CINTAS</span>
					<span class="category-eye"><img src="library/images/ico-eye.svg"></span>
				</a>
			</div>
		</div>
	</section>

	<!-- Productos destacados -->
	<section class="o-featured-products">
		<div class="container cont-1300">
			<h2 class="title-destacado"><span>UN STOCK A LA ALTURA DE LOS PROFESIONALES</span>Productos Destacados</h2>

			<div class="o-featured-products__list">
				<div class="o-products">
					<a href="" class="o-products__img">
						<img src="library/images/products-1.png" alt="Titulo Producto">
					</a>
					<div class="o-products__info">
						<span class="o-products__category">Maquinaria</span>
						<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
						<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
					</div>

					<!-- Caja flotante oculta con la descripcion y botones de compra -->
					<div class="o-products__desc">
						<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
						<div class="o-products__desc__info">
							<span><img src="library/images/ico-envio-producto.svg">Envío gratuito</span>
							<span><img src="library/images/ico-entrega-producto.svg">Entrega en 24h.</span>
						</div>
						<div class="o-products__desc__pay">
							<a href="" class="see-products">Ver producto</a>
							<button class="add-product"></button>
						</div>
					</div>
				</div>

				<div class="o-products">
					<a href="" class="o-products__img">
						<img src="library/images/products-1.png" alt="Titulo Producto">
					</a>
					<div class="o-products__info">
						<span class="o-products__category">Maquinaria</span>
						<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
						<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
					</div>

					<!-- Caja flotante oculta con la descripcion y botones de compra -->
					<div class="o-products__desc">
						<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
						<div class="o-products__desc__info">
							<span><img src="library/images/ico-envio-producto.svg">Envío gratuito</span>
							<span><img src="library/images/ico-entrega-producto.svg">Entrega en 24h.</span>
						</div>
						<div class="o-products__desc__pay">
							<a href="" class="see-products">Ver producto</a>
							<button class="add-product"></button>
						</div>
					</div>
				</div>

				<div class="o-products">
					<a href="" class="o-products__img">
						<img src="library/images/products-1.png" alt="Titulo Producto">
					</a>
					<div class="o-products__info">
						<span class="o-products__category">Maquinaria</span>
						<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
						<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
					</div>

					<!-- Caja flotante oculta con la descripcion y botones de compra -->
					<div class="o-products__desc">
						<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
						<div class="o-products__desc__info">
							<span><img src="library/images/ico-envio-producto.svg">Envío gratuito</span>
							<span><img src="library/images/ico-entrega-producto.svg">Entrega en 24h.</span>
						</div>
						<div class="o-products__desc__pay">
							<a href="" class="see-products">Ver producto</a>
							<button class="add-product"></button>
						</div>
					</div>
				</div>

				<div class="o-products">
					<a href="" class="o-products__img">
						<img src="library/images/products-1.png" alt="Titulo Producto">
					</a>
					<div class="o-products__info">
						<span class="o-products__category">Maquinaria</span>
						<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
						<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
					</div>

					<!-- Caja flotante oculta con la descripcion y botones de compra -->
					<div class="o-products__desc">
						<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
						<div class="o-products__desc__info">
							<span><img src="library/images/ico-envio-producto.svg">Envío gratuito</span>
							<span><img src="library/images/ico-entrega-producto.svg">Entrega en 24h.</span>
						</div>
						<div class="o-products__desc__pay">
							<a href="" class="see-products">Ver producto</a>
							<button class="add-product"></button>
						</div>
					</div>
				</div>
				<div class="o-products">
					<a href="" class="o-products__img">
						<img src="library/images/products-1.png" alt="Titulo Producto">
					</a>
					<div class="o-products__info">
						<span class="o-products__category">Maquinaria</span>
						<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
						<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
					</div>

					<!-- Caja flotante oculta con la descripcion y botones de compra -->
					<div class="o-products__desc">
						<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
						<div class="o-products__desc__info">
							<span><img src="library/images/ico-envio-producto.svg">Envío gratuito</span>
							<span><img src="library/images/ico-entrega-producto.svg">Entrega en 24h.</span>
						</div>
						<div class="o-products__desc__pay">
							<a href="" class="see-products">Ver producto</a>
							<button class="add-product"></button>
						</div>
					</div>
				</div>

				<div class="o-products">
					<a href="" class="o-products__img">
						<img src="library/images/products-1.png" alt="Titulo Producto">
					</a>
					<div class="o-products__info">
						<span class="o-products__category">Maquinaria</span>
						<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
						<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
					</div>

					<!-- Caja flotante oculta con la descripcion y botones de compra -->
					<div class="o-products__desc">
						<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
					</div>
				</div>

				<div class="o-products">
					<a href="" class="o-products__img">
						<img src="library/images/products-1.png" alt="Titulo Producto">
					</a>
					<div class="o-products__info">
						<span class="o-products__category">Maquinaria</span>
						<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
						<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
					</div>

					<!-- Caja flotante oculta con la descripcion y botones de compra -->
					<div class="o-products__desc">
						<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
					</div>
				</div>

				<div class="o-products">
					<a href="" class="o-products__img">
						<img src="library/images/products-1.png" alt="Titulo Producto">
					</a>
					<div class="o-products__info">
						<span class="o-products__category">Maquinaria</span>
						<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
						<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
					</div>

					<!-- Caja flotante oculta con la descripcion y botones de compra -->
					<div class="o-products__desc">
						<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
					</div>
				</div>

				<div class="o-products">
					<a href="" class="o-products__img">
						<img src="library/images/products-1.png" alt="Titulo Producto">
					</a>
					<div class="o-products__info">
						<span class="o-products__category">Maquinaria</span>
						<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
						<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
					</div>

					<!-- Caja flotante oculta con la descripcion y botones de compra -->
					<div class="o-products__desc">
						<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
					</div>
				</div>

				<div class="o-products">
					<a href="" class="o-products__img">
						<img src="library/images/products-1.png" alt="Titulo Producto">
					</a>
					<div class="o-products__info">
						<span class="o-products__category">Maquinaria</span>
						<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
						<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
					</div>

					<!-- Caja flotante oculta con la descripcion y botones de compra -->
					<div class="o-products__desc">
						<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
					</div>
				</div>

				<div class="o-products">
					<a href="" class="o-products__img">
						<img src="library/images/products-1.png" alt="Titulo Producto">
					</a>
					<div class="o-products__info">
						<span class="o-products__category">Maquinaria</span>
						<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
						<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
					</div>

					<!-- Caja flotante oculta con la descripcion y botones de compra -->
					<div class="o-products__desc">
						<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
					</div>
				</div>

				<div class="o-products">
					<a href="" class="o-products__img">
						<img src="library/images/products-1.png" alt="Titulo Producto">
					</a>
					<div class="o-products__info">
						<span class="o-products__category">Maquinaria</span>
						<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
						<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
					</div>

					<!-- Caja flotante oculta con la descripcion y botones de compra -->
					<div class="o-products__desc">
						<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- BANNERS HOME -->
	<section class="o-home-banners">
		<div class="container cont-1300">
			<div class="o-home-banners__grid">
				<div class="banners-1 banners" style="">
					<h3 class="title">¡OFERTAS ESPECIALES!</h3>
					<span class="offer">20%<span>DESCUENTO</span></span>
					<p>En todas nuestras marcas</p>
					<a href="" class="btn-yellow">Ver productos</a>
				</div>
				<div class="banners-2 banners">
					<h3 class="title">¡ÚLTIMAS UNIDADES!</h3>
					<span>Atornillador Impacto</span>
				</div>
				<div class="banners-3 banners">
					<img src="library/images/ico-entrega.svg">
					<h3 class="title">ENVÍOS GRATIS A PARTIR DE 60€</h3>
					<p>Lorem ipsum in dolor set amet consecuetum</p>
				</div>
				<div class="banners-4 banners">
					<h3 class="title">CONOCE LA NUEVA SIERRA CIRCULAR</h3>
					<span class="price">211,35 €</span>
					<a href="" class="btn-eye"><img src="library/images/ico-eye.svg"></a>
				</div>
				<div class="banners-5 banners">
					<span>18 NOV</span>
					<h3 class="title">BLACK FRIDAY</h3>
					<a href="" class="btn-yellow">Ver OFERTAS</a>
				</div>
			</div>
		</div>
	</section>

	<section class="o-marcas">
		<div class="container cont-1300">
			<h3 class="o-marcas__title">Nuestras marcas</h3>

			<div class="o-marcas__slider">
				<p><img src="library/images/marca-1.jpg"></p>
				<p><img src="library/images/marca-2.jpg"></p>
				<p><img src="library/images/marca-3.jpg"></p>
				<p><img src="library/images/marca-4.jpg"></p>
				<p><img src="library/images/marca-5.jpg"></p>
				<p><img src="library/images/marca-2.jpg"></p>
				<p><img src="library/images/marca-3.jpg"></p>
			</div>
		</div>
	</section>

	<section class="o-form-home">
		<div class="form-box">
			<h4 class="form-box__subtitle">¿TIENES ALGUNA DUDA?</h4>
			<h3 class="form-box__title">¿TIENES ALGUNA DUDA?</h3>
			<form action="">
				<div class="grid-form-3">
					<label>
						Nombre
						<input type="text">
					</label>
					<label>
						Teléfono
						<input type="tel">
					</label>
					<label>
						E-mail
						<input type="email">
					</label>
				</div>
				<label>
					Comentarios
					<textarea name="" id="" cols="30" rows="10"></textarea>
				</label>
				<label class="politicas"><input type="checkbox">He leído y acepto los <a href="">términos y condiciones</a></label>
				<input type="submit" vlaue="ENVIAR">
			</form>
		</div>
	</section>
</main>

{/block}

