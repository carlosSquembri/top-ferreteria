<?php

require_once _PS_ROOT_DIR_.'/override/classes/my_dir/Panelprincipal.php';


class AdminPanelprincipalController extends AdminController {

  public function __construct(){

    $this->bootstrap = true;
    $this->show_toolbar = false;
    $this->context = Context::getContext();
    $this->table = 'panelprincipal'; // SQL table name, will be prefixed with _DB_PREFIX_
    $this->identifier = 'id'; // SQL column to be used as primary key
    $this->className = 'Panelprincipal'; // PHP class name
    parent::__construct();
  }
  public function processSave(){
    parent::processSave();
    
    if(Validate::isLoadedObject($this->object)){

      $this->object->id=1;

      if($_FILES['imagen']['name']!=""){
        $newName = "{$this->object->id}-".$_FILES['imagen']['name'];
        $uploader = new \Uploader('imagen');
        $uploader->setSavePath(_PS_IMG_DIR_.'imagenesslider');
        $files = $uploader->process($newName);
        $this->object->imagen = $newName;
      }

      $this->object->titulo=$_POST['titulo'];
      $this->object->descripcion=$_POST['descripcion'];
      $this->object->precio=$_POST['precio'];
      $this->object->enlace=$_POST['enlace'];
      
      if($_POST['passwd']!=""){
        $this->object->passwd=md5(_COOKIE_KEY_.$_POST['passwd']);
      }
      $this->object->save();
    }
  }

  public function getDatos(){
    $consulta = 'SELECT * from ps_panelprincipal where id="1"';
    $resultado = Db::getInstance()->ExecuteS($consulta);
    return $resultado;
  }

  public function getImagen(){
    $consulta = 'SELECT imagen from ps_panelprincipal where id="1"';
    $resultado = Db::getInstance()->ExecuteS($consulta);
    return $resultado;
  }

  public function renderForm()
  {

      $datos = $this->getDatos();

      $image_url=false;
      $id_actual = 1;
      if($this->getImagen($id_actual)[0]['imagen']!=""){
        $var_img='http://picpiclab.com/img/imagenesslider/'.$this->getImagen($id_actual)[0]['imagen'];
        $image_url='<img style="max-width: 400px;height: auto;border: 1px solid #c9d6da;border-radius: 5px;padding: 4px;margin-bottom: 10px;" src="'.$var_img.'"';
      }else{
        $image_url=false;
      }

      $this->fields_value = array(
          'titulo' => $datos[0]["titulo"],
          'descripcion' => $datos[0]["descripcion"],
          'precio' => $datos[0]["precio"],
          'enlace' => $datos[0]["enlace"]
      );

      $this->fields_form = [
        'legend' => [
          'title' => 'Panel principal Home',
          'icon' => 'icon-filter'
        ],
        'input' => [
          ['name'=>'titulo','type'=>'text','label'=>'Título','required'=>true,'desc' => 'Título de la promoción'],
          ['name'=>'descripcion','type'=>'textarea','label'=>'Descripción de la promoción','autoload_rte' => true,'desc' => 'Introduce un texto que describa la promoción.'],
          ['name'=>'precio','type'=>'text','label'=>'Precio','desc' => 'Precio de la promoción, puedes poner el texto que quieras, ejemplo: "DESDE 30,50€ - 26 PÁGINAS"'],
          ['name'=>'enlace','type'=>'text','label'=>'Enlace botón promoción','required'=>true,'desc' => 'Url a la que redirige el botón'],
          ['name'=>'imagen','type'=>'file','label'=>'Imagen','desc' => 'Introduce la imagen de la promoción','hint' => $this->l('Tamaño recomendado 798px x 823px para su correcta visualización.'),'display_image' => true, 'image'=>$image_url ? $image_url : false,],
        ],
        'submit' => [
          'title' => $this->trans('Save', [], 'Admin.Actions'),
        ]
      ];  

      return parent::renderForm();
  }

  public function initContent()
  {
      $this->display = 'view';
      $this->page_header_toolbar_title = $this->toolbar_title = 'Patterns design sample';

      parent::initContent();

      $this->content .= $this->renderForm();

      $this->context->smarty->assign(array('content' => $this->content));
  }

}