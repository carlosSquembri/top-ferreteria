<?php include('includes/header.php'); ?>

<main class="c-contacto">
	
	<div class="o-contacto-head">
		<div class="container-portada">
			<div class="o-contacto-head__box">
				<h2 class="o-contacto-head__subtitle">¿TIENES ALGUNA DUDA?</h2>
				<h1 class="o-contacto-head__title">Contacto y Faq’s</h1>
			</div>
		</div>
	</div>

	<section class="o-contacto">
		<div class="container cont-1300">
			<div class="o-contacto__box">
				<div class="o-contacto__box__top">
					<button id="open-contact" class="active">Formulario de contacto</button>
					<button id="open-faqs">Preguntas frecuentes</button>
				</div>
				
				<div class="o-contacto-form">
					<form action="">
						<div class="grid-form-3">
							<label>
								Nombre
								<input type="text">
							</label>
							<label>
								Teléfono
								<input type="tel">
							</label>
							<label>
								E-mail
								<input type="email">
							</label>
						</div>

						<label>
							Comentarios
							<textarea name="" id="" cols="30" rows="10"></textarea>
						</label>
						
						<div class="form-flex">
							<label class="acceptance-politicas">
								<input type="checkbox">
								He leído y acepto los <a href="">términos y condiciones</a>
							</label>
							<input type="submit" value="Enviar">
						</div>
					</form>
				</div>

				<div class="o-contacto-faqs" style="display: none;">
					<button class="accordion">Duis vestibulum elit vel neque pharetra vulputate. Quisque scelerisque nisi urna. Duis rutrum non risus in imperdiet. Proin molestie accumsan nulla sit amet mattis. Ut vel tristique neque.</button>
					<div class="panel">
					  <p>Curabitur sit amet eros eu arcu consectetur pulvinar. Aliquam sodales, turpis eget tristique tempor, sapien lacus facilisis diam, molestie efficitur sapien velit nec magna. Maecenas interdum efficitur tempor. Quisque scelerisque id odio nec dictum. Donec sed pulvinar tortor. Duis ut dolor consectetur, mollis lorem et, mattis est.</p>
					  <p>Maecenas interdum efficitur tempor. Quisque scelerisque id odio nec dictum. Donec sed pulvinar tortor. Duis ut dolor consectetur, mollis lorem et, mattis est.</p>
					</div>

					<button class="accordion">Proin ex ipsum, facilisis id tincidunt sed, vulputate in lacus. Donec pharetra faucibus leo, vitae vestibulum2</button>
					<div class="panel">
					  <p>Curabitur sit amet eros eu arcu consectetur pulvinar. Aliquam sodales, turpis eget tristique tempor, sapien lacus facilisis diam, molestie efficitur sapien velit nec magna. Maecenas interdum efficitur tempor. Quisque scelerisque id odio nec dictum. Donec sed pulvinar tortor. Duis ut dolor consectetur, mollis lorem et, mattis est.</p>
					</div>

					<button class="accordion">Integer ac interdum lacus. Nunc porta semper lacus a varius. Pellentesque habitant morbi tristique</button>
					<div class="panel">
					  <p>Curabitur sit amet eros eu arcu consectetur pulvinar. Aliquam sodales, turpis eget tristique tempor, sapien lacus facilisis diam, molestie efficitur sapien velit nec magna. Maecenas interdum efficitur tempor. Quisque scelerisque id odio nec dictum. Donec sed pulvinar tortor. Duis ut dolor consectetur, mollis lorem et, mattis est.</p>
					</div>
				</div>
				
			</div>
		</div>
	</section>

</main>

<?php include('includes/footer.php'); ?>