<body>
	<header class="c-header">
		<div class="header-top">
			<div class="container cont-1300">
				<span><?php svg('ico-monedero-header.svg'); ?>¡Precios super, super, super ajustados!</span>

				<div class="">
					<a href="mailto:info@topferreteria.es"><?php svg('ico-mail-header.svg'); ?>info@topferreteria.es</a>
					<a href="tel:694904053"><?php svg('ico-tel-header.svg'); ?>+34 694 904 053</a>
				</div>
			</div>
		</div>

		<div id="menu">
			<?php include('includes/pre-menu-mobile.php'); ?>
			<div class="container cont-1300">
				<a href="index.php" id="logo">
					<?php svg('logo.svg') ?>
				</a>
				<div id="nav-menu">
					<ul>
						<li>
							<a href="categoria.php">CONSUMIBLES</a>
							<ul class="submenu">
								<li><a class="active" href="">FIJACIÓN Y ANCLAJES</a></li>
								<li><a href="">CINTAS Y ADHESIVOS</a></li>
								<li><a href="">PINTURAS Y ACEITES</a></li>
								<li><a href="">CUERDAS Y CINTAS</a></li>

								<img src="library/images/sombra-submenu.svg" class="sombra">
								<img src="library/images/tornillo-submenu.png" class="tornillo">
							</ul>
						</li>
						<li><a href="categoria.php">MAQUINARIA</a></li>
						<li>
							<a href="categoria.php">HERRAMIENTAS</a>
							<ul class="submenu">
								<li><a class="active" href="">FIJACIÓN Y ANCLAJES</a></li>
								<li><a href="">CINTAS Y ADHESIVOS</a></li>
								<li><a href="">PINTURAS Y ACEITES</a></li>
								<li><a href="">CUERDAS Y CINTAS</a></li>

								<img src="library/images/sombra-submenu.svg" class="sombra">
								<img src="library/images/tornillo-submenu.png" class="tornillo">
							</ul>
						</li>
						<li><a href="page-contacto.php">CONTACTO</a></li>
					</ul>

					<div class="menu-user">
						<a href="" class="ico-user"><?php svg('ico-user.svg') ?></a>
						<a href="" class="cart-header">
							<?php svg('ico-cart.svg') ?>
							<span>15</span>
						</a>
						<ul class="submenu">
							<li><a class="active" href="">REGISTRARSE</a></li>
							<li><a href="">LOGIN</a></li>
						</ul>
					</div>

					<div class="header-search">
						<input type="text" id="header-search" onchange="textoVacio()" onkeydown="buscar();" class="active" placeholder="Buscar…">
					</div>
				</div>
			</div>
		</div>
	</header>