<div class="container cont-1300">
	<div class="o-footer-top">
		<div class="o-footer-top__categorys">
			<h3>Categorías:</h3>
			<ul>
				<li><a href="">Fijación y anclajes</a></li>
				<li><a href="">Herramienta eléctrica</a></li>
				<li><a href="">Corte taladro y cepillería</a></li>
				<li><a href="">Protección laboral y seguridad</a></li>
				<li><a href="">Pinceles y aceites</a></li>
				<li><a href="">Cintas y adhesivos</a></li>
				<li><a href="">Herramienta manual</a></li>
				<li><a href="">Escaleras</a></li>
				<li><a href="">Herramienta taller</a></li>
				<li><a href="">Soldadura</a></li>
				<li><a href="">Medición y escritura</a></li>
				<li><a href="">Cuerdas y cintas</a></li>
			</ul>
		</div>
		<div class="o-footer-top__info">
			<div class="info-top">
				<a href="mailto:info@topferreteria.es"><?php svg('ico-mail-header.svg'); ?>info@topferreteria.es</a>
				<a href="tel:+ 34 694 904 053"><?php svg('ico-tel-header.svg'); ?>+ 34 694 904 053</a>
			</div>
			<div class="info-pay">
				<span>Formas de pago:</span>
				<img src="library/images/formas-pago.png" alt="Formas de pago">
			</div>
		</div>
	</div>
	<div class="sombra">
		<?php svg('sombra.svg'); ?>
	</div>
	<div class="o-footer-bottom">
		<a href="" id="logo-footer"><?php svg('logo-footer.svg') ?></a>
		<ul class="politicas">
			<li><a href="">Aviso legal</a></li>
			<li><span>|</span></li>
			<li><a href="">Política de privacidad</a></li>
			<li><span>|</span></li>
			<li><a href="">Política de cookies</a></li>
		</ul>
	</div>
</div>