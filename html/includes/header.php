<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Top Ferreteria</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,500,600,800&display=swap" rel="stylesheet">
	<!-- SLICK -->
	<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick-theme.css"/>
	<!-- <link rel="stylesheet" href="library/css/bootstrap.min.css"> -->
	<link rel="stylesheet" href="library/css/style.css">

	<!-- Sharethis -->
	<script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=5a1ff332689cdf0012ad4a52&product=sop' async='async'></script>
	<?php include('includes/functions.php'); ?>

	<!-- range slider -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.0/css/ion.rangeSlider.min.css"/>


</head>

<?php include('includes/menu-comun.php'); ?>

<!-- BUSCADOR -->
<div class="search-back" style="display: none;"></div>
<div id="search" class="search" style="display: none;">
	<div class="search__filters">
		<div class="search__filters__container">
			<span class="title">FILTROS</span>

    		<input type="text" class="js-range-slider" name="my_range" value="" />

			<div class="filters-selects">
				<div class="style-select">
					<select name="" id="">
						<option value="">Marcas</option>
						<option value="">Marcas</option>
						<option value="">Marcas</option>
						<option value="">Marcas</option>
					</select>
				</div>

				<div class="style-select">
					<select name="" id="">
						<option value="">Consumibles</option>
						<option value="">Consumibles</option>
						<option value="">Consumibles</option>
						<option value="">Consumibles</option>
					</select>
				</div>

				<div class="style-select">
					<select name="" id="">
						<option value="">Maquinaria</option>
						<option value="">Maquinaria</option>
						<option value="">Maquinaria</option>
						<option value="">Maquinaria</option>
					</select>
				</div>
				<div class="style-select">
					<select name="" id="">
						<option value="">Herramientas</option>
						<option value="">Herramientas</option>
						<option value="">Herramientas</option>
						<option value="">Herramientas</option>
					</select>
				</div>
			</div>

			<button class="close-search">
				<?php svg('ico-close-search.svg'); ?>
			</button>
		</div>
	</div>

	<div class="search__content">
		<span class="search__content__title">47 Resultados encontrados</span>

		<div class="search__content__box">
			<div class="search__content__list">
				<div class="o-products">
					<a href="" class="o-products__img">
						<img src="library/images/products-1.png" alt="Titulo Producto">
					</a>
					<div class="o-products__info">
						<span class="o-products__category">Maquinaria</span>
						<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
						<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
					</div>

					<!-- Caja flotante oculta con la descripcion y botones de compra -->
					<div class="o-products__desc">
						<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
					</div>
				</div>

				<div class="o-products">
					<a href="" class="o-products__img">
						<img src="library/images/products-1.png" alt="Titulo Producto">
					</a>
					<div class="o-products__info">
						<span class="o-products__category">Maquinaria</span>
						<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
						<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
					</div>

					<!-- Caja flotante oculta con la descripcion y botones de compra -->
					<div class="o-products__desc">
						<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
					</div>
				</div>

				<div class="o-products">
					<a href="" class="o-products__img">
						<img src="library/images/products-1.png" alt="Titulo Producto">
					</a>
					<div class="o-products__info">
						<span class="o-products__category">Maquinaria</span>
						<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
						<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
					</div>

					<!-- Caja flotante oculta con la descripcion y botones de compra -->
					<div class="o-products__desc">
						<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
					</div>
				</div>

				<div class="o-products">
					<a href="" class="o-products__img">
						<img src="library/images/products-1.png" alt="Titulo Producto">
					</a>
					<div class="o-products__info">
						<span class="o-products__category">Maquinaria</span>
						<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
						<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
					</div>

					<!-- Caja flotante oculta con la descripcion y botones de compra -->
					<div class="o-products__desc">
						<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
					</div>
				</div>
				<div class="o-products">
					<a href="" class="o-products__img">
						<img src="library/images/products-1.png" alt="Titulo Producto">
					</a>
					<div class="o-products__info">
						<span class="o-products__category">Maquinaria</span>
						<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
						<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
					</div>

					<!-- Caja flotante oculta con la descripcion y botones de compra -->
					<div class="o-products__desc">
						<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
					</div>
				</div>

				<div class="o-products">
					<a href="" class="o-products__img">
						<img src="library/images/products-1.png" alt="Titulo Producto">
					</a>
					<div class="o-products__info">
						<span class="o-products__category">Maquinaria</span>
						<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
						<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
					</div>

					<!-- Caja flotante oculta con la descripcion y botones de compra -->
					<div class="o-products__desc">
						<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
					</div>
				</div>

				<div class="o-products">
					<a href="" class="o-products__img">
						<img src="library/images/products-1.png" alt="Titulo Producto">
					</a>
					<div class="o-products__info">
						<span class="o-products__category">Maquinaria</span>
						<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
						<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
					</div>

					<!-- Caja flotante oculta con la descripcion y botones de compra -->
					<div class="o-products__desc">
						<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
					</div>
				</div>

				<div class="o-products">
					<a href="" class="o-products__img">
						<img src="library/images/products-1.png" alt="Titulo Producto">
					</a>
					<div class="o-products__info">
						<span class="o-products__category">Maquinaria</span>
						<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
						<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
					</div>

					<!-- Caja flotante oculta con la descripcion y botones de compra -->
					<div class="o-products__desc">
						<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>