<section class="pre-menu-mobile">
  <div class="container cont-1300">
    <div class="mobile-menu-left">
        <a href="mailto:info@topferreteria.es"><?php svg('ico-mail-header.svg'); ?></a>
        <a href="tel:694904053"><?php svg('ico-tel-header.svg'); ?></a>
    </div>

    <article class="logo-mobile">
      <a href="index.php" title="logo">
        <?php svg('logo.svg') ?>
      </a>
    </article>

    <div class="mobile-menu-right">
      <button calss="header-search" id="header-search"><?php svg('ico-search-mobile.svg'); ?></button>
      <article class="btn-menu">
        <span></span>
        <span></span>
        <span></span>
      </article>
    </div>

  </div>
</section>
