<?php include('includes/header.php'); ?>

<main class="c-categoria">
	
	<div class="o-categoria-head">
		<div class="container-portada">
			<div class="o-categoria-head__box">
				<h2 class="o-categoria-head__subtitle">SUBCATEGORÍA, SUBCATEGORÍA</h2>
				<h1 class="o-categoria-head__title">Categoría de producto</h1>
			</div>
		</div>
	</div>

	<section class="o-categoria-desc">
		<div class="container cont-1400">
			<div class="cont-hide">
				<ul class="breadcrumbs">
					<li><a href="index.php"><img src="library/images/ico-home.svg">Inicio  / </a></li>
					<li>Categoría de producto</li>
				</ul>
				<div class="gradient"></div>
				<div class="btn-more"></div>
				<p>Maecenas interdum lorem eleifend orci aliquam mollis. Aliquam non rhoncus magna. Suspendisse aliquet tincidunt enim, ut commodo elit feugiat et. Maecenas nec enim quis diam faucibus tristique. Nam fermentum, ipsum in suscipit pharetra, mi odio aliquet neque, non iaculis augue elit et libero. Phasellus tempor faucibus faucibus. Sed eu mauris sem. Etiam et varius felis. Donec et libero vitae mauris consectetur egestas. Fusce aliquet, augue non efficitur sodales, turpis nisl consectetur sem, at rutrum lectus libero quis elit. Suspendisse cursus laoreet sapien, in lobortis justo posuere vitae. Aliquam commodo posuere tellus in lacinia. Nullam ac eleifend odio. Donec aliquam semper tellus a condimentum.</p>
			</div>
		</div>
	</section>

	<section class="o-categoria">
		<div class="container cont-1400">
			<button class="btn-open-filters">FILTROS</button>
			<div class="o-categoria__sidebar">
				<div class="close-sidebar__box">
					<button class="close-sidebar">
						<span></span>
						<span></span>
					</button>
				</div>
				
				<div class="top-sidebar">
					<span class="title">FILTROS</span>
					<button class="btn-borrar">Borrar filtros</button>
				</div>
				
				<div class="filters__range">
					<input type="text" class="js-range-slider" name="my_range" value="" />
				</div>

				<div class="filters">
					<div class="style-select">
						<select name="" id="">
							<option value="">Marcas</option>
							<option value="">Marcas</option>
							<option value="">Marcas</option>
						</select>
					</div>
					<div class="style-select">
						<select name="" id="">
							<option value="">Consumibles</option>
							<option value="">Consumibles</option>
							<option value="">Consumibles</option>
						</select>
					</div>
					<div class="style-select">
						<select name="" id="">
							<option value="">Maquinaria</option>
							<option value="">Maquinaria</option>
							<option value="">Maquinaria</option>
						</select>
					</div>
					<div class="style-select">
						<select name="" id="">
							<option value="">Herramientas</option>
							<option value="">Herramientas</option>
							<option value="">Herramientas</option>
						</select>
					</div>
					<div class="style-select">
						<select name="" id="">
							<option value="">Ordenar por</option>
							<option value="">Ordenar por</option>
							<option value="">Ordenar por</option>
						</select>
					</div>
				</div>

				<div class="filters">
					<h3>Subcategoría</h3>
					<div class="style-select">
						<select name="" id="">
							<option value="">Tornillos</option>
							<option value="">Tornillos</option>
							<option value="">Tornillos</option>
						</select>
					</div>
					<div class="style-select">
						<select name="" id="">
							<option value="">Arandelas</option>
							<option value="">Arandelas</option>
							<option value="">Arandelas</option>
						</select>
					</div>
					<div class="style-select">
						<select name="" id="">
							<option value="">Tuercas</option>
							<option value="">Tuercas</option>
							<option value="">Tuercas</option>
						</select>
					</div>
					<div class="style-select">
						<select name="" id="">
							<option value="">Tacos</option>
							<option value="">Tacos</option>
							<option value="">Tacos</option>
						</select>
					</div>
					<div class="style-select">
						<select name="" id="">
							<option value="">Bridas y tacos brida</option>
							<option value="">Bridas y tacos brida</option>
							<option value="">Bridas y tacos brida</option>
						</select>
					</div>
					<div class="style-select">
						<select name="" id="">
							<option value="">Abrazaderas</option>
							<option value="">Abrazaderas</option>
							<option value="">Abrazaderas</option>
						</select>
					</div>
				</div>

				<a href="" class="banner-img"><img src="library/images/banner-sidebar.png"></a>

				<ul class="info-sidebar">
					<li>
						<img src="library/images/ico-envio.svg">
						<div>
							<h3>Envíos gratis a partir de 60€*</h3>
							<span>*Excepto en artículos pesados</span>
						</div>
					</li>

					<li>
						<img src="library/images/ico-candado.svg">
						<div>
							<h3>Esta página es segura</h3>
							<span>para tus compras</span>
						</div>
					</li>

					<li>
						<img src="library/images/ico-billetera.svg">
						<div>
							<h3>Formas de pago disponibles</h3>
							<span>PayPal, Mastercard, Visa</span>
						</div>
					</li>
				</ul>
			</div>
			<div class="o-categoria__list">
				<span class="total-results">47 Resultados encontrados</span>

				<div class="o-categoria__list__grid">
					<div class="o-products">
						<span class="o-products-offert">OFERTA</span>
						<a href="" class="o-products__img">
							<img src="library/images/products-1.png" alt="Titulo Producto">
						</a>
						<div class="o-products__info">
							<span class="o-products__category">Maquinaria</span>
							<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
							<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
						</div>

						<!-- Caja flotante oculta con la descripcion y botones de compra -->
						<div class="o-products__desc">
							<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
							<div class="o-products__desc__info">
								<span><?php svg('ico-envio-producto.svg'); ?>Envío gratuito</span>
								<span><?php svg('ico-entrega-producto.svg'); ?>Entrega en 24h.</span>
							</div>
							<div class="o-products__desc__pay">
								<a href="" class="see-products">Ver producto</a>
								<button class="add-product"></button>
							</div>
						</div>
					</div>

					<div class="o-products">
						<span class="o-products-new">NUEVO</span>
						<a href="" class="o-products__img">
							<img src="library/images/products-1.png" alt="Titulo Producto">
						</a>
						<div class="o-products__info">
							<span class="o-products__category">Maquinaria</span>
							<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
							<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
						</div>

						<!-- Caja flotante oculta con la descripcion y botones de compra -->
						<div class="o-products__desc">
							<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
							<div class="o-products__desc__info">
								<span><?php svg('ico-envio-producto.svg'); ?>Envío gratuito</span>
								<span><?php svg('ico-entrega-producto.svg'); ?>Entrega en 24h.</span>
							</div>
							<div class="o-products__desc__pay">
								<a href="" class="see-products">Ver producto</a>
								<button class="add-product"></button>
							</div>
						</div>
					</div>

					<div class="o-products">
						<a href="" class="o-products__img">
							<img src="library/images/products-1.png" alt="Titulo Producto">
						</a>
						<div class="o-products__info">
							<span class="o-products__category">Maquinaria</span>
							<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
							<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
						</div>

						<!-- Caja flotante oculta con la descripcion y botones de compra -->
						<div class="o-products__desc">
							<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
							<div class="o-products__desc__info">
								<span><?php svg('ico-envio-producto.svg'); ?>Envío gratuito</span>
								<span><?php svg('ico-entrega-producto.svg'); ?>Entrega en 24h.</span>
							</div>
							<div class="o-products__desc__pay">
								<a href="" class="see-products">Ver producto</a>
								<button class="add-product"></button>
							</div>
						</div>
					</div>

					<div class="o-products">
						<a href="" class="o-products__img">
							<img src="library/images/products-1.png" alt="Titulo Producto">
						</a>
						<div class="o-products__info">
							<span class="o-products__category">Maquinaria</span>
							<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
							<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
						</div>

						<!-- Caja flotante oculta con la descripcion y botones de compra -->
						<div class="o-products__desc">
							<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
							<div class="o-products__desc__info">
								<span><?php svg('ico-envio-producto.svg'); ?>Envío gratuito</span>
								<span><?php svg('ico-entrega-producto.svg'); ?>Entrega en 24h.</span>
							</div>
							<div class="o-products__desc__pay">
								<a href="" class="see-products">Ver producto</a>
								<button class="add-product"></button>
							</div>
						</div>
					</div>

					<div class="o-products">
						<a href="" class="o-products__img">
							<img src="library/images/products-1.png" alt="Titulo Producto">
						</a>
						<div class="o-products__info">
							<span class="o-products__category">Maquinaria</span>
							<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
							<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
						</div>

						<!-- Caja flotante oculta con la descripcion y botones de compra -->
						<div class="o-products__desc">
							<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
							<div class="o-products__desc__info">
								<span><?php svg('ico-envio-producto.svg'); ?>Envío gratuito</span>
								<span><?php svg('ico-entrega-producto.svg'); ?>Entrega en 24h.</span>
							</div>
							<div class="o-products__desc__pay">
								<a href="" class="see-products">Ver producto</a>
								<button class="add-product"></button>
							</div>
						</div>
					</div>

					<div class="o-products">
						<a href="" class="o-products__img">
							<img src="library/images/products-1.png" alt="Titulo Producto">
						</a>
						<div class="o-products__info">
							<span class="o-products__category">Maquinaria</span>
							<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
							<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
						</div>

						<!-- Caja flotante oculta con la descripcion y botones de compra -->
						<div class="o-products__desc">
							<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
							<div class="o-products__desc__info">
								<span><?php svg('ico-envio-producto.svg'); ?>Envío gratuito</span>
								<span><?php svg('ico-entrega-producto.svg'); ?>Entrega en 24h.</span>
							</div>
							<div class="o-products__desc__pay">
								<a href="" class="see-products">Ver producto</a>
								<button class="add-product"></button>
							</div>
						</div>
					</div>

					<div class="o-products">
						<a href="" class="o-products__img">
							<img src="library/images/products-1.png" alt="Titulo Producto">
						</a>
						<div class="o-products__info">
							<span class="o-products__category">Maquinaria</span>
							<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
							<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
						</div>

						<!-- Caja flotante oculta con la descripcion y botones de compra -->
						<div class="o-products__desc">
							<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
							<div class="o-products__desc__info">
								<span><?php svg('ico-envio-producto.svg'); ?>Envío gratuito</span>
								<span><?php svg('ico-entrega-producto.svg'); ?>Entrega en 24h.</span>
							</div>
							<div class="o-products__desc__pay">
								<a href="" class="see-products">Ver producto</a>
								<button class="add-product"></button>
							</div>
						</div>
					</div>

					<div class="o-products">
						<a href="" class="o-products__img">
							<img src="library/images/products-1.png" alt="Titulo Producto">
						</a>
						<div class="o-products__info">
							<span class="o-products__category">Maquinaria</span>
							<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
							<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
						</div>

						<!-- Caja flotante oculta con la descripcion y botones de compra -->
						<div class="o-products__desc">
							<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
							<div class="o-products__desc__info">
								<span><?php svg('ico-envio-producto.svg'); ?>Envío gratuito</span>
								<span><?php svg('ico-entrega-producto.svg'); ?>Entrega en 24h.</span>
							</div>
							<div class="o-products__desc__pay">
								<a href="" class="see-products">Ver producto</a>
								<button class="add-product"></button>
							</div>
						</div>
					</div>

					<div class="o-products">
						<a href="" class="o-products__img">
							<img src="library/images/products-1.png" alt="Titulo Producto">
						</a>
						<div class="o-products__info">
							<span class="o-products__category">Maquinaria</span>
							<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
							<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
						</div>

						<!-- Caja flotante oculta con la descripcion y botones de compra -->
						<div class="o-products__desc">
							<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
							<div class="o-products__desc__info">
								<span><?php svg('ico-envio-producto.svg'); ?>Envío gratuito</span>
								<span><?php svg('ico-entrega-producto.svg'); ?>Entrega en 24h.</span>
							</div>
							<div class="o-products__desc__pay">
								<a href="" class="see-products">Ver producto</a>
								<button class="add-product"></button>
							</div>
						</div>
					</div>

					<div class="o-products">
						<a href="" class="o-products__img">
							<img src="library/images/products-1.png" alt="Titulo Producto">
						</a>
						<div class="o-products__info">
							<span class="o-products__category">Maquinaria</span>
							<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
							<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
						</div>

						<!-- Caja flotante oculta con la descripcion y botones de compra -->
						<div class="o-products__desc">
							<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
							<div class="o-products__desc__info">
								<span><?php svg('ico-envio-producto.svg'); ?>Envío gratuito</span>
								<span><?php svg('ico-entrega-producto.svg'); ?>Entrega en 24h.</span>
							</div>
							<div class="o-products__desc__pay">
								<a href="" class="see-products">Ver producto</a>
								<button class="add-product"></button>
							</div>
						</div>
					</div>

					<div class="o-products">
						<a href="" class="o-products__img">
							<img src="library/images/products-1.png" alt="Titulo Producto">
						</a>
						<div class="o-products__info">
							<span class="o-products__category">Maquinaria</span>
							<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
							<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
						</div>

						<!-- Caja flotante oculta con la descripcion y botones de compra -->
						<div class="o-products__desc">
							<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
							<div class="o-products__desc__info">
								<span><?php svg('ico-envio-producto.svg'); ?>Envío gratuito</span>
								<span><?php svg('ico-entrega-producto.svg'); ?>Entrega en 24h.</span>
							</div>
							<div class="o-products__desc__pay">
								<a href="" class="see-products">Ver producto</a>
								<button class="add-product"></button>
							</div>
						</div>
					</div>

					<div class="o-products">
						<a href="" class="o-products__img">
							<img src="library/images/products-1.png" alt="Titulo Producto">
						</a>
						<div class="o-products__info">
							<span class="o-products__category">Maquinaria</span>
							<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
							<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
						</div>

						<!-- Caja flotante oculta con la descripcion y botones de compra -->
						<div class="o-products__desc">
							<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
							<div class="o-products__desc__info">
								<span><?php svg('ico-envio-producto.svg'); ?>Envío gratuito</span>
								<span><?php svg('ico-entrega-producto.svg'); ?>Entrega en 24h.</span>
							</div>
							<div class="o-products__desc__pay">
								<a href="" class="see-products">Ver producto</a>
								<button class="add-product"></button>
							</div>
						</div>
					</div>
				</div>

				<ul class="pagination">
					<ul>
						<li><a href="" class="prev"></a></li>
						<li><a href="" class="active">1</a></li>
						<li><a href="">2</a></li>
						<li><a href="">3</a></li>
						<li><a href="">4</a></li>
						<li>...</li>
						<li><a href="">5</a></li>
						<li><a href="" class="next"></a></li>
					</ul>
					
				</ul>
			</div>
		</div>
	</section>

</main>

<?php include('includes/footer.php'); ?>