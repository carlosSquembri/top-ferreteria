<?php include('includes/header.php'); ?>

<main class="c-categoria">
	
	<section class="o-single-product">
		<div class="container cont-1300">
			<ul class="breadcrumbs">
				<li><a href="index.php"><img src="library/images/ico-home.svg">Inicio  / </a></li>
				<li><a href="categoria.php">Categoría de producto  / </a></li>
				<li>Atornillador impacto 100NM 230W TD0101F</li>
			</ul>

			<div class="o-single-product__grid">
				<div class="o-single-product__grid__slider">
					<div class="o-single-product__grid__slider__grid">
						<div class="slider-productos-left">
							<div><img src="library/images/slider-1.jpg"></div>
							<div><img src="library/images/slider-2.jpg"></div>
							<div><img src="library/images/slider-3.jpg"></div>
							<div><img src="library/images/slider-1.jpg"></div>
						</div>
						<div class="slider-productos-right">
							<div><img src="library/images/slider-1.jpg"></div>
							<div><img src="library/images/slider-2.jpg"></div>
							<div><img src="library/images/slider-3.jpg"></div>
							<div><img src="library/images/slider-1.jpg"></div>
						</div>
					</div>
				</div>
				<div class="o-single-product__grid__info">
					<div class="flex-top">
						<h4>Marca: <img src="library/images/marca.jpg"></h4>
						<h4>Ref: 992032</h4>
					</div>
					<h1 class="title">ATORNILLADOR IMPACTO 100NM 230W TD0101F</h1>
					<h3 class="price"><span class="old-price">158,00 €</span>108,00 €<span class="impuestos">1 ud.</span></h3>
					<div class="desc">
						<p>Duis porta, ligula rhoncus euismod pretium, nisi tellus eleifend odio, luctus viverra sem dolor id sem. Maecenas a venenatis enim, quis porttitor magna. Etiam nec rhoncus neque. Sed quis ultrices eros.</p>
						<p>Curabitur sit amet eros eu arcu consectetur pulvinar. Aliquam sodales, turpis eget tristique tempor, sapien lacus facilisis diam, molestie efficitur sapien velit nec magna. Maecenas interdum efficitur tempor. Quisque scelerisque id odio nec dictum. Donec sed pulvinar tortor. Duis ut dolor consectetur, mollis lorem et, mattis est.</p>
					</div>

					<div class="total-product">
						<span>Unidades:</span>
						<input type="number" value="0">
						<a href="" class="btn-add">AÑADIR AL CARRITO</a>
					</div>
				</div>
			</div>

			<div class="o-single-product__tabs">
				<ul class="o-single-product__tabs__buttons">
					<li><button id="open-prestaciones"><?php svg('prestaciones.svg'); ?>Prestaciones</button></li>
					<li><button id="open-especificaciones"><?php svg('especificaciones.svg'); ?>Especificaciones técnicas</button></li>
					<li><button id="open-equipo"><?php svg('equipo.svg'); ?>Equipo</button></li>
				</ul>
				<div class="o-single-product__tabs__textarea" id="prestaciones-info" style="display: none;">
					<p>Duis porta, ligula rhoncus euismod pretium, nisi tellus eleifend odio, luctus viverra sem dolor id sem. Maecenas a venenatis enim, quis porttitor magna. Etiam nec rhoncus neque. Sed quis ultrices eros.</p>

					<p>Curabitur sit amet eros eu arcu consectetur pulvinar. Aliquam sodales, turpis eget tristique tempor, sapien lacus facilisis diam, molestie efficitur sapien velit nec magna. Maecenas interdum efficitur tempor. Quisque scelerisque id odio nec dictum. Donec sed pulvinar tortor. Duis ut dolor consectetur, mollis lorem et, mattis est.</p>
				</div>
				<div class="o-single-product__tabs__textarea" id="especificaciones-info" style="display: none;">
					<p>Duis porta, ligula rhoncus euismod pretium, nisi tellus eleifend odio, luctus viverra sem dolor id sem. Maecenas a venenatis enim, quis porttitor magna. Etiam nec rhoncus neque. Sed quis ultrices eros.</p>

					<p>Curabitur sit amet eros eu arcu consectetur pulvinar. Aliquam sodales, turpis eget tristique tempor, sapien lacus facilisis diam, molestie efficitur sapien velit nec magna. Maecenas interdum efficitur tempor. Quisque scelerisque id odio nec dictum. Donec sed pulvinar tortor. Duis ut dolor consectetur, mollis lorem et, mattis est.</p>
				</div>
				<div class="o-single-product__tabs__textarea" id="equipo-info" style="display: none;">
					<p>Duis porta, ligula rhoncus euismod pretium, nisi tellus eleifend odio, luctus viverra sem dolor id sem. Maecenas a venenatis enim, quis porttitor magna. Etiam nec rhoncus neque. Sed quis ultrices eros.</p>

					<p>Curabitur sit amet eros eu arcu consectetur pulvinar. Aliquam sodales, turpis eget tristique tempor, sapien lacus facilisis diam, molestie efficitur sapien velit nec magna. Maecenas interdum efficitur tempor. Quisque scelerisque id odio nec dictum. Donec sed pulvinar tortor. Duis ut dolor consectetur, mollis lorem et, mattis est.</p>
				</div>
			</div>
		</div>
	</section>

	<!-- Productos destacados -->
	<section class="o-featured-products">
		<div class="container cont-1300">
			<h2 class="title-destacado">Productos relacionados</h2>

			<div class="o-featured-products__list">
				<div class="o-products">
					<a href="" class="o-products__img">
						<img src="library/images/products-1.png" alt="Titulo Producto">
					</a>
					<div class="o-products__info">
						<span class="o-products__category">Maquinaria</span>
						<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
						<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
					</div>

					<!-- Caja flotante oculta con la descripcion y botones de compra -->
					<div class="o-products__desc">
						<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
					</div>
				</div>

				<div class="o-products">
					<a href="" class="o-products__img">
						<img src="library/images/products-1.png" alt="Titulo Producto">
					</a>
					<div class="o-products__info">
						<span class="o-products__category">Maquinaria</span>
						<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
						<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
					</div>

					<!-- Caja flotante oculta con la descripcion y botones de compra -->
					<div class="o-products__desc">
						<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
					</div>
				</div>

				<div class="o-products">
					<a href="" class="o-products__img">
						<img src="library/images/products-1.png" alt="Titulo Producto">
					</a>
					<div class="o-products__info">
						<span class="o-products__category">Maquinaria</span>
						<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
						<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
					</div>

					<!-- Caja flotante oculta con la descripcion y botones de compra -->
					<div class="o-products__desc">
						<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
					</div>
				</div>

				<div class="o-products">
					<a href="" class="o-products__img">
						<img src="library/images/products-1.png" alt="Titulo Producto">
					</a>
					<div class="o-products__info">
						<span class="o-products__category">Maquinaria</span>
						<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
						<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
					</div>

					<!-- Caja flotante oculta con la descripcion y botones de compra -->
					<div class="o-products__desc">
						<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
					</div>
				</div>
				<div class="o-products">
					<a href="" class="o-products__img">
						<img src="library/images/products-1.png" alt="Titulo Producto">
					</a>
					<div class="o-products__info">
						<span class="o-products__category">Maquinaria</span>
						<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
						<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
					</div>

					<!-- Caja flotante oculta con la descripcion y botones de compra -->
					<div class="o-products__desc">
						<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
					</div>
				</div>

				<div class="o-products">
					<a href="" class="o-products__img">
						<img src="library/images/products-1.png" alt="Titulo Producto">
					</a>
					<div class="o-products__info">
						<span class="o-products__category">Maquinaria</span>
						<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
						<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
					</div>

					<!-- Caja flotante oculta con la descripcion y botones de compra -->
					<div class="o-products__desc">
						<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
					</div>
				</div>

				<div class="o-products">
					<a href="" class="o-products__img">
						<img src="library/images/products-1.png" alt="Titulo Producto">
					</a>
					<div class="o-products__info">
						<span class="o-products__category">Maquinaria</span>
						<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
						<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
					</div>

					<!-- Caja flotante oculta con la descripcion y botones de compra -->
					<div class="o-products__desc">
						<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
					</div>
				</div>

				<div class="o-products">
					<a href="" class="o-products__img">
						<img src="library/images/products-1.png" alt="Titulo Producto">
					</a>
					<div class="o-products__info">
						<span class="o-products__category">Maquinaria</span>
						<a href="" class="o-products__title">TALADRO COMBINADO<span>10.8V 30NM 2.0AH</span></a>
						<span class="o-products__price"><span class="old-price">158,00 €</span>199,96 €</span>
					</div>

					<!-- Caja flotante oculta con la descripcion y botones de compra -->
					<div class="o-products__desc">
						<p>Taladro combinado de 10,8 V y 1,1 Kg de peso. Con posición de atornillado, taladrado en madera o metal y taladrado en obra. Se suministra con baterías de 2.0 Ah…</p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="o-marcas">
		<div class="container cont-1300">
			<h3 class="o-marcas__title">Nuestras marcas</h3>

			<div class="o-marcas__slider">
				<p><img src="library/images/marca-1.jpg"></p>
				<p><img src="library/images/marca-2.jpg"></p>
				<p><img src="library/images/marca-3.jpg"></p>
				<p><img src="library/images/marca-4.jpg"></p>
				<p><img src="library/images/marca-5.jpg"></p>
				<p><img src="library/images/marca-2.jpg"></p>
				<p><img src="library/images/marca-3.jpg"></p>
			</div>
		</div>
	</section>

</main>

<?php include('includes/footer.php'); ?>