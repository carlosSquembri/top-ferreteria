<?php include('includes/header.php'); ?>

<div class="mod-blog">
	<div class="container cont-960">
		<section class="mod-first-post">
			<h1>
				<a href="" title="Título">Euismod tincidunt ut laoreet dolore magna aliquam erat </a>
			</h1>
			<div class="img">
				<a href="" title="Título">
					<img src="library/images/img-post.jpg" alt="Producto">
				</a>
				<p class="cat-fecha">
					<span>Categoría //</span>
					Enero 2018
				</p>
			</div>
			<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros ugue duis dolore te feugait nulla facilisi.Lorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut </p>
		</section>
		<section class="mod-listado-post">
			<div class="listado-post col-md-9">
				<?php for ($i=0; $i < 6; $i++) { ?>				
					<article class="col-xs-6">
						<div class="img">
							<a href="" title="Título">
								<img src="library/images/img-post-listado1.jpg" alt="Producto">
							</a>
							<p class="cat-fecha">
								<span>Categoría //</span>
								Enero 2018
							</p>
						</div>
						<div class="text">
							<h3>
								<a href="" title="Título">Euismod tincidunt ut laoreet qu </a>
							</h3>
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna  </p>
						</div>
					</article>
				<?php } ?>
			</div>
			<div class="sidebar col-md-3">
				<h4>Categorías</h4>
				<ul>
					<li>
						<a href="" title="">Magna aliquam erat</a>
					</li>
					<li>
						<a href="" title="">Magna aliquam erat</a>
					</li>
					<li>
						<a href="" title="">Magna aliquam erat</a>
					</li>
					<li>
						<a href="" title="">Magna aliquam erat</a>
					</li>
					<li>
						<a href="" title="">Magna aliquam erat</a>
					</li>
				</ul>
			</div>
		</section>
		<section class="mod-paginacion">
			<ol>
				<li class="current">
					<a href="">1</a>
				</li>
				<li>
					<a href="">2</a>
				</li>
				<li>
					<a href="">3</a>
				</li>
			</ol>
		</section>
	</div>
</div>

<?php include('includes/footer.php'); ?>